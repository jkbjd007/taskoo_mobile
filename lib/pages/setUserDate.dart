import 'package:shared_preferences/shared_preferences.dart';
class authModel{

  static setPrefs_Authenticate({int id,String email,String phone,String name,String userId,String imageUrl,String token,bool islogin}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("id", id);
    prefs.setString("email", email);
    prefs.setString("name", name);
    prefs.setString("phone", phone);
    prefs.setString("userId", userId);
    prefs.setString("imageUrl", imageUrl);
    prefs.setString("token", token);
    prefs.setBool("islogin", islogin);
  }
  static getId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('id');
  }
  static getUserEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('email');
  }
  static getUserName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('name');
  }
  static getUserPhone() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('phone');
  }
  static getUserId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('userId');
  }
  static getUserImageUrl() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('imageUrl');
  }
  static getUserToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }
  static checkIsLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('islogin');
  }

}