import 'package:flutter/material.dart';
import 'package:taskoo/pages/profile.dart';
import 'package:taskoo/repeatedWidgets/CustomAppBar.dart';
import 'package:taskoo/repeatedWidgets/CustomBottomBar.dart';
import 'package:taskoo/res/color.dart';
import 'package:page_transition/page_transition.dart';
import 'package:taskoo/res/string.dart';
import 'package:taskoo/res/style.dart';

import '../main.dart';
import 'browserTasks.dart';
import 'myTasks.dart';
class notifications extends StatefulWidget {
  @override
  _notificationsState createState() => _notificationsState();
}

class _notificationsState extends State<notifications> {
  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  int select = 4;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffold,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100),
        child: CustomAppBarSingleColor(
          parentContext: context,
          centerWigets: Text(notification,style: appBarStyle),
          hight: 100,
        ),
      ),
      body: Container(),
      bottomNavigationBar: CustomBottomBar(select=4),
    );
  }

}
