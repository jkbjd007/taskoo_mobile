import 'package:flutter/material.dart';
import 'package:taskoo/repeatedWidgets/CustomButton.dart';
import 'package:taskoo/repeatedWidgets/CustomTextField.dart';
import 'package:taskoo/repeatedWidgets/CutomContainerTime.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/size.dart';
import 'package:taskoo/res/string.dart';
import 'package:taskoo/res/style.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;
class serachTasks extends StatefulWidget {
  @override
  _serachTasksState createState() => _serachTasksState();
}

class _serachTasksState extends State<serachTasks> {
  List tasktype = [
    {"txt": inPerson, "id": 1, "select": true},
    {"txt": remote, "id": 2, "select": false},
    {"txt": all, "id": 3, "select": false},
  ];
  bool serachdetails = false;
  bool showAvailibe = true;
  double distance = 1.0009;
  double lowPrice = 500.0009;
  double maxPrice = 15000.0009;
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(
            left: size.convert(context, 10),
            right: size.convert(context, 10),
            top: size.convert(context, 10)),
        child: Column(
          children: <Widget>[
            Container(
              child: ListView.separated(
                //crossAxisCount: 1
                itemCount: tasktype.length,
                physics: ScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: GestureDetector(
                      onTap: () {
                        changeTaskType(index);
                      },
                      child: Stack(
                        children: <Widget>[
                          CutomContainerTime(
                            height: size.convert(context, 55),
                            width: size.convertWithWidth(context, 395),
                            txt1: tasktype[index]["txt"],
                            selectColor: tasktype[index]["select"]
                                ? appColor
                                : darkblack.withOpacity(0.05),
                          ),
                        ],
                      ),
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(height: size.convert(context, 5));
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: size.convert(context, 15),
                  left: size.convert(context, 10)),
              child: Row(
                children: <Widget>[
                  Text(
                    localityLable,
                    style: labelStyle,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: size.convert(context, 7)),
              child: CustomTextField(
                color1: darkblack,
                onchange: (val) {},
                hints: localityHints,
                trailingIcon: Icon(
                  Icons.search,
                  color: darkblack,
                  size: size.convert(context, 30),
                ),
                borderwidth: 1,
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: size.convert(context, 20),
                  left: size.convert(context, 10)),
              child: Row(
                children: <Widget>[
                  Text(distanceLabel, style: labelStyle),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: size.convert(context, 20),
                  left: size.convert(context, 10)),
              child: Text(distance.toString().substring(0, 4) + distanceMeasure,
                  style: distanceHintsStyle),
            ),
            Container(
              padding: EdgeInsets.only(left: size.convert(context, 17)),
              child: SliderTheme(
                data: SliderThemeData(
                  trackHeight: 2.5,
                  overlayShape: RoundSliderOverlayShape(overlayRadius: 0.01),
                ),
                child: Slider(
                  label: distance.toString().substring(0, 4),
                  value: distance,
                  onChanged: (val) {
                    setState(() {
                      distance = val;
                      if (val == 1) {
                        distance = distance + 0.0001;
                      }
                    });
                  },
                  min: 1,
                  max: 100.0001,
                  activeColor: appColor,
                  inactiveColor: darkblack,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: size.convert(context, 20),
                  left: size.convert(context, 10)),
              child: Text(
                  lowPrice.toStringAsFixed(1) +
                      "${currencySign} - " +
                      maxPrice.toStringAsFixed(1) +
                      currencySign,
                  style: distanceHintsStyle),
            ),
            Container(
                child: SliderTheme(
                  data: SliderThemeData(
                      activeTrackColor: appColor,
                      trackHeight: 2.5,
                      inactiveTrackColor: darkblack,
                      thumbColor: appColor,
                      overlayColor: Colors.transparent,
                      overlayShape: RoundSliderOverlayShape(overlayRadius: 0.02)),
                  child: frs.RangeSlider(
                    lowerValue: lowPrice,
                    upperValue: maxPrice,
                    max: 22000,
                    min: 100,
                    allowThumbOverlap: true,
                    onChanged: (low, max) {
                      setState(() {
                        lowPrice = low;
                        maxPrice = max;
                      });
                    },
                  ),
                )),
            Container(
                margin: EdgeInsets.only(top: size.convert(context, 20)),
                padding:
                EdgeInsets.symmetric(horizontal: size.convert(context, 13)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: size.convertWithHeight(context, 42),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            availableTasks,
                            style: TextStyle(
                                fontSize: size.convert(context, 20),
                                fontFamily: "HelveticaNeue",
                                fontWeight: FontWeight.w600,
                                color: Colors.black87),
                          ),
                          Text(
                            "Hide tasks those are already assigned or completed",
                            style: TextStyle(
                                fontSize: size.convert(context, 10),
                                fontFamily: "HelveticaNeue",
                                fontWeight: FontWeight.w600,
                                color: Colors.black87),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: size.convertWithHeight(context, 42),
                      child: _toggleButton(),)
                  ],
                )),
            Container(
                margin: EdgeInsets.only(top: size.convert(context, 30)),
                child: GestureDetector(
                    onTap: () {
                      print("Apply serach............");
                      setState(() {
                        serachdetails = false;
                      });

                    },
                    child: CustomButton(
                      txt: "apply",
                      hight: size.convert(context, 60),
                      filledColor: appColor,
                    ))),
            // Container(child:RangeSliderData(),),
          ],
        ),
      ),
    );
  }
  changeTaskType(int index) {
    int i = 0;
    setState(() {
      tasktype.forEach((val) {
        if (index == i) {
          print("same $i $index");
          val["select"] = true;
        } else {
          val["select"] = false;
        }
        i = i + 1;
      });
    });
  }

  _toggleButton() {
    return Container(
      width: size.convert(context, 42),
      height: size.convert(context, 20),
      decoration: BoxDecoration(
          color: darkblack,
          borderRadius: BorderRadius.circular(size.convert(context, 10))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          !showAvailibe
              ? GestureDetector(
            onTap: () {
              setState(() {
                showAvailibe = !showAvailibe;
              });
            },
            child: Container(
              height: size.convert(context, 20),
              width: size.convert(context, 25),
              decoration: BoxDecoration(
                  borderRadius:
                  BorderRadius.circular(size.convert(context, 10)),
                  color: whiteColor),
            ),
          )
              : Container(
            height: 20,
            width: 15,
          ),
          showAvailibe
              ? GestureDetector(
            onTap: () {
              setState(() {
                showAvailibe = !showAvailibe;
              });
            },
            child: Container(
              height: size.convert(context, 20),
              width: size.convert(context, 25),
              decoration: BoxDecoration(
                borderRadius:
                BorderRadius.circular(size.convert(context, 10)),
                color: appColor,
              ),
            ),
          )
              : Container(
            height: 20,
            width: 15,
          ),

        ],
      ),
    );
  }
}
