import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/size.dart';
import 'package:taskoo/res/style.dart';
class otpTextField extends StatelessWidget {
  Widget WIcon;
  Color selectColor ;
  Color borderColor ;
  double height;
  double width;
  Color color1;
  Widget iconWidget;
  Function onchange;
  Function ontap;
  String hints;
  Widget trailingIcon ;
  double borderwidth;
  int maxline;
  int maxlength;
  double contHight;
  bool enable = true;
  Function onsave;
  bool error ;
  TextEditingController textEditingController;
  Function onvalidate;
  otpTextField({this.borderColor,this.width,this.height,this.selectColor,this.WIcon,
    this.error = false,this.onvalidate,this.hints,this.trailingIcon,this.borderwidth,
    this.color1,this.onsave,this.textEditingController,this.ontap,this.maxline,
    this.onchange,this.enable,this.contHight,this.iconWidget,this.maxlength});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height == null ? size.convert(context, 50) : height,
      width: width ==null ? size.convert(context, 274): width,
      decoration: BoxDecoration(
          color: selectColor ==null ? whiteColor: selectColor,
          borderRadius: BorderRadius.circular(size.convert(context, 10)),
          border: Border.all(
            color: borderColor??darkblack,
            width: 1,
          )
      ),
      child:  TextFormField(
        validator: onvalidate?? (val){},
        onSaved: onsave ==null ? onsave :(val){
          print(val);
        },
        textAlign: TextAlign.center,
          keyboardType: TextInputType.number,
        onChanged: onchange == null ? (e){} : onchange,
        enabled: enable,
        onTap: ontap == null ? (){} : ontap,
        textInputAction: TextInputAction.done,
        maxLines: maxline==null ? 1 :maxline,
        controller: textEditingController,
        decoration: InputDecoration(
          disabledBorder: InputBorder.none,
          border: InputBorder.none,
          hintText: hints == null ? "": hints,
          hintStyle: textFieldHintsStyle,
        ),
        style: textFieldStyle,
        cursorColor: Colors.black,
      ),
    );
  }
}
