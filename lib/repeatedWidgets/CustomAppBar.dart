import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/size.dart';
class CustomAppBar extends StatelessWidget {
  BuildContext parentContext;
  Color color1;
  Color color2;
  Widget leadingIcon;
  Widget trailingIcon;
  Widget centerWigets;
  double hight;
  CustomAppBar({
   @required this.parentContext,this.color1,this.color2,this.leadingIcon,
    this.trailingIcon,this.centerWigets, @required this.hight,
});



  @override
  Widget build(BuildContext context) {
    //Size size = MediaQuery.of(context).size;
    return Container(
      height: hight,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [color1,color2],
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
        )
      ),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 15,right: 30, left: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            leadingIcon == null ?SizedBox(width: 5,) : leadingIcon ,
            centerWigets == null ?SizedBox(width: 5,) : centerWigets ,
            trailingIcon == null ?SizedBox(width: 5,) : trailingIcon ,
          ],
        ),
      ),
    );
  }
}

class CustomAppBarSingleColor extends StatelessWidget {
  BuildContext parentContext;
  Color color1;
  Color color2;
  Widget leadingIcon;
  Widget trailingIcon;
  Widget centerWigets;
  double hight;
  CustomAppBarSingleColor({
    @required this.parentContext,this.color1,this.color2,this.leadingIcon,
    this.trailingIcon,this.centerWigets, @required this.hight,
  });



  @override
  Widget build(BuildContext context) {
    Size size1 = MediaQuery.of(context).size;
    return Container(
      height: hight > 90 ? size.convert(context, 80) : hight,
      decoration: BoxDecoration(
      border: Border(
        bottom: BorderSide(
          width: 0.5,
          color: Color(0xffA3A3A3)
        )
      ),
      color: color1 == null ? darkblack.withOpacity(0.05) : color1,
      ),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 5,right: 30, left: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            leadingIcon == null ?SizedBox(width: 5,) : leadingIcon ,
            centerWigets == null ?SizedBox(width: 5,) : centerWigets ,
            trailingIcon == null ?SizedBox(width: 5,) : trailingIcon ,
          ],
        ),
      ),
    );
  }

}
