import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:taskoo/pages/setUserDate.dart';

class serverRequest {
  Dio dio = new Dio();
  Future<bool> checkInternet() async {
    try{
      var result = await InternetAddress.lookup('google.com');
      return true;
    }
    on SocketException catch (e){
      return false;
    }
  }
  Future<dynamic> postMethod(String url, Map<dynamic, dynamic> body,
      {String param = ""}) async {
    String token = await authModel.getUserToken();
    print("Token = $token");
    print(url + param);
    try {
      print(json.encode(body));
      var result = await InternetAddress.lookup('google.com');
      Response response = await dio.post(url,data: json.encode(body));
//      headers: {
//        "Authorization": "Bearer $token",
//    }
      print("reponse code = "+response.statusCode.toString());
      return response;
    } on SocketException catch (e) {
      print(e.toString());
      return e;
    } catch (ex) {
      print(ex.toString());
      return ("error! $ex");
    }
  }
  Future<dynamic> getMethod(String url,{Map <dynamic,dynamic> param}) async {
    print(url);
    try{
      print("before server call");
      var checkInternet = await InternetAddress.lookup('google.com');
      Response response =  await dio.get(url);
      print("/////////////////////////////");
      print(response.statusCode.toString());
      print(response.statusCode.toString());
      print("/////////////////////////////");
      if(response.statusCode <= 201){
        print(response.statusCode.toString());
        return (response);
      }
    }
    on SocketException catch (ex){
      return ex;
    }
    catch(e){
      print("print "+e);
      return e;
    }
  }

}
