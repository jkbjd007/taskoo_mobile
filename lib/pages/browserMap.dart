import 'dart:convert';
import 'dart:io';

import 'package:date_format/date_format.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:rxdart/subjects.dart';
import 'package:taskoo/model/Geometry.dart';
import 'package:taskoo/model/Location.dart';
import 'package:taskoo/model/PostTask.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;
import 'package:taskoo/repeatedWidgets/CustomAppBar.dart';
import 'package:taskoo/repeatedWidgets/CustomBottomBar.dart';
import 'package:taskoo/repeatedWidgets/CustomButton.dart';
import 'package:taskoo/repeatedWidgets/CustomTextField.dart';
import 'package:taskoo/repeatedWidgets/CutomContainerTime.dart';
import 'package:taskoo/repeatedWidgets/serachTasks.dart';
import 'package:taskoo/repeatedWidgets/snackbar.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/serverCall.dart';
import 'package:taskoo/res/size.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:taskoo/res/string.dart';
import 'package:taskoo/res/style.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:rxdart/rxdart.dart';
class browserMap extends StatefulWidget {
  List<PostTask> taskInMap=[];
  locationData locationdata;

  browserMap({this.taskInMap,this.locationdata});
  @override
  _browserMapState createState() => _browserMapState();
}

class _browserMapState extends State<browserMap> {
  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
 int select = 3;
  PlacesAutocompleteResponse _autocompleteResponse;
  final _queryBehavior = BehaviorSubject<String>.seeded('');
  TextEditingController _localityController = TextEditingController();
  GoogleMapsPlaces _places;
  ValueChanged<PlacesAutocompleteResponse> onError;
  PlacesDetailsResponse _placesDetailsResponse;
  bool dropdown = false;
  serverRequest _serverRequest = serverRequest();
  List tasktypeList = [
    {"txt": inPerson, "id": 1, "select": true},
    {"txt": remote, "id": 2, "select": false},
    {"txt": all, "id": 3, "select": false},
  ];
  bool onlyAvailibeTask = true; /// ts = 2 only available tasks empty for all in api
  double distance = 1.0009; /// d for distance in api
  double lowPrice = 500.0009; /// minp lower limit in api
  double maxPrice = 15000.0009; /// maxp upper limit in api
  String _vicinity = "";  /// l in api
  int taskType = 1;/// tt 1 for in person 2 for remote 3 for all
  bool serachdetails = false;
  List data= [textSearchLocation,price];
  Completer<GoogleMapController> _controller = Completer();
  List <PostTask> task= [];
   CameraPosition _kGooglePlex ;
  Set<Marker> markList ={};
  var api;

  bool loading = true;

  readfile() async {
    String data = await rootBundle
        .loadString("assets/propertyfile/EndPointApi.json")
        .whenComplete(() {
      setState(() {
        loading = false;
      });
    });
    api = jsonDecode(data);

  }

  Marker maker1 ;
   CameraPosition _kLake;
  setData({bool isserach}){
    if(!isserach){
      task = widget.taskInMap;
    }
    if(task!=null){
      if(widget.taskInMap.length>=1){
        _kGooglePlex = CameraPosition(
          target: LatLng(task[0].location.geometry.lat, task[0].location.geometry.lng),
          zoom: 15.1,
        );
        _kLake = CameraPosition(
            bearing: task[0].location.geometry.lat,
            target: LatLng(task[0].location.geometry.lat, task[0].location.geometry.lng),
            tilt: task[0].location.geometry.lng,
            zoom: 50.081409);
        int i=0;
        task.forEach((mark){
          if(mark.location.placeId != null){
            i++;
            print("marker ${i}");
            markList.add(Marker(
              markerId: MarkerId(mark.location.placeId),
              position: LatLng(mark.location.geometry.lat, mark.location.geometry.lng),
              infoWindow: InfoWindow(title: mark.location.name),
              icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
            ));
          }
        });
      }
    }

    if(!isserach && widget.locationdata!=null){
      print("one location are not null ");
      _kGooglePlex = CameraPosition(
        target: LatLng(widget.locationdata.geometry.lat, widget.locationdata.geometry.lng),
        zoom: 15.1,
      );
      markList.add( Marker(
        markerId: MarkerId(widget.locationdata.name),
        position: LatLng(widget.locationdata.geometry.lat, widget.locationdata.geometry.lng),
        infoWindow: InfoWindow(title: widget.locationdata.name),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
      ));
      _kLake = CameraPosition(
          bearing: widget.locationdata.geometry.lat,
          target: LatLng(widget.locationdata.geometry.lat, widget.locationdata.geometry.lng),
          tilt: widget.locationdata.geometry.lng,
          zoom: 150.081409);
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setData(isserach: false);
    readfile();
    _places = GoogleMapsPlaces(apiKey: Googlekey);

    _queryBehavior.stream
        .debounceTime(const Duration(milliseconds: 300))
        .listen(doSearch);
    _localityController.addListener(() {
      _queryBehavior.add(_localityController.text);
    });
  }
  @override
  Widget build(BuildContext context) {
    print(widget.taskInMap);
    return Scaffold(
      key: _scaffold,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 100)),
        child: CustomAppBarSingleColor(
          color1: whiteColor,
          parentContext: context,
          centerWigets: Text(serachdetails?applyFilter:browseTasks,style: appBarStyle,),
          hight: size.convert(context, 100),
          leadingIcon: serachdetails?Container(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  serachdetails = false;
                });
              },
              child: Icon(
                Icons.clear,
                color: darkblack,
                size: size.convert(context, 28),
              ),
            ),
          ):GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back,color: Colors.black,
            ),
          ),
        ),
      ),
      body: Container(
        child: loading?Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: size.convert(context, 100),
                height: size.convert(context, 100),
                child: LoadingIndicator(
                  color: appColor,
                  indicatorType: Indicator.ballScale,
                ),
              ),
            ],
          ),
        ):_body(),
      ),
      bottomNavigationBar: serachdetails?null:CustomBottomBar(select=3),
    );
  }
  _body(){
    return serachdetails?serachBody():Container(
      color: whiteColor,
      child: Column(
        children: <Widget>[
          _topsearch(),
          SizedBox(height: size.convert(context, 10),),
          _showMap(),
        ],
      ),
    );
  }
  _topsearch(){
    Size size1 = MediaQuery.of(context).size;
    return Container(
      color: whiteColor,
      height: size1.longestSide*0.058565153,
      padding: EdgeInsets.only(bottom: size1.longestSide*0.006641288,
          left: size1.longestSide*0.014641,right:size1.longestSide*0.014641),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Expanded(
            child: Container(
              //color: Colors.blue,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(child: Image.asset("assets/icons/Icon_feather_filter.png",height: size1.longestSide*0.02635431,
                    width: size1.longestSide*0.02928257,
                    fit: BoxFit.cover,color: appColor,),),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: size.convert(context, 10) ),
                      child: ListView.separated(
                        itemBuilder: (BuildContext context, int index){
                          return Align(alignment: Alignment.bottomLeft,
                              child: GestureDetector(
                                onTap: (){
                                  setState(() {
                                    serachdetails = true;
                                  });
                                },
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: size1.longestSide*0.01464128,
                                      vertical: size1.longestSide*0.00432064421),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(size.convert(context, 20)),
                                      color: appColor
                                  ),
                                  child: Text(data[index],
                                    style: filterTextStyle,),),
                              ));
                        },
                        separatorBuilder: (BuildContext context, int index){
                          return SizedBox(width: size.convert(context, 5),);
                        },
                        itemCount: data.length,
                        physics: ScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                      ),
                    ),
                  ),
                ],
              ),),
          ),
          GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: Container(child: SvgPicture.asset("assets/svg_Icons/list-h.svg",
              height: size.convert(context, 25),
              width: size.convert(context, 40),
              fit: BoxFit.cover,color: appColor,),),
          ),
        ],
      ),
    );
  }
  _showMap(){
    return Expanded(
      child: Container(
        child: GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: _kGooglePlex,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
          markers: markList??maker1,),
      ),
    );

  }

  serachBody() {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(
            left: size.convert(context, 10),
            right: size.convert(context, 10),
            top: size.convert(context, 10)),
        child: Column(
          children: <Widget>[
            Container(
              child: ListView.separated(
                //crossAxisCount: 1
                itemCount: tasktypeList.length,
                physics: ScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: GestureDetector(
                      onTap: () {
                        changeTaskType(index);
                      },
                      child: Stack(
                        children: <Widget>[
                          CutomContainerTime(
                            height: size.convert(context, 55),
                            width: size.convertWithWidth(context, 395),
                            txt1: tasktypeList[index]["txt"],
                            selectColor: tasktypeList[index]["select"]
                                ? appColor
                                : darkblack.withOpacity(0.05),
                          ),
                        ],
                      ),
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return SizedBox(height: size.convert(context, 5));
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: size.convert(context, 15),
                  left: size.convert(context, 10)),
              child: Row(
                children: <Widget>[
                  Text(
                    localityLable,
                    style: labelStyle,
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: size.convert(context, 7)),
              child: CustomTextField(
                color1: darkblack,
                onchange: (val) {},
                textEditingController: _localityController,
                ontap: () async {
                  setState(() {
                    dropdown = true;
                  });
                },
                onsave: () {
                  setState(() {
                    dropdown = false;
                  });
                },

                hints: localityHints,
                trailingIcon: SvgPicture.asset("assets/svg_Icons/search.svg",),
                borderwidth: 1,
              ),
            ),
            dropdown
                ? AnimatedContainer(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convert(context, 5)),
              padding: EdgeInsets.only(top: size.convert(context, 20)),
              color: whiteColor,
              duration: Duration(microseconds: 3),
              child: _autocompleteResponse == null
                  ? SizedBox()
                  : ListView.separated(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          selectlatilng(_autocompleteResponse
                              .predictions[index].placeId);
                          _localityController.text =
                              _autocompleteResponse
                                  .predictions[index]
                                  .structuredFormatting
                                  .mainText;
                          _autocompleteResponse = null;
                          dropdown = false;
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: size.convert(context, 10)),
                        child: Row(
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              IcoFontIcons.locationPin,
                              color: darkblack,
                              size: size.convert(context, 20),
                            ),
                            SizedBox(
                              width: size.convert(context, 1),
                            ),
                            Expanded(
                              child: Text(
                                _autocompleteResponse
                                    .predictions[index]
                                    .structuredFormatting
                                    .mainText,
                                style: TextStyle(
                                  fontSize:
                                  size.convert(context, 20),
                                  fontFamily: "HelveticaNeue",
                                  color: darkblack,
                                  //    fontWeight: FontWeight.w600
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder:
                      (BuildContext context, int index) {
                    return SizedBox(
                      height: 12,
                    );
                  },
                  itemCount:
                  _autocompleteResponse.predictions.length),
            )
                : SizedBox(),
            Container(
              margin: EdgeInsets.only(
                  top: size.convert(context, 20),
                  left: size.convert(context, 10)),
              child: Row(
                children: <Widget>[
                  Text(distanceLabel, style: labelStyle),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: size.convert(context, 20),
                  left: size.convert(context, 10)),
              child: Text(distance.toString().substring(0, 4) + distanceMeasure,
                  style: distanceHintsStyle),
            ),
            Container(
              padding: EdgeInsets.only(left: size.convert(context, 17)),
              child: SliderTheme(
                data: SliderThemeData(
                  trackHeight: 2.5,
                  overlayShape: RoundSliderOverlayShape(overlayRadius: 0.01),
                ),
                child: Slider(
                  label: distance.toString().substring(0, 4),
                  value: distance,
                  onChanged: (val) {
                    setState(() {
                      distance = val;
                      if (val == 1) {
                        distance = distance + 0.0001;
                      }
                    });
                  },
                  min: 1,
                  max: 100.0001,
                  activeColor: appColor,
                  inactiveColor: darkblack,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: size.convert(context, 20),
                  left: size.convert(context, 10)),
              child: Text(
                  lowPrice.toStringAsFixed(1) +
                      "${currencySign} - " +
                      maxPrice.toStringAsFixed(1) +
                      currencySign,
                  style: distanceHintsStyle),
            ),
            Container(
                child: SliderTheme(
                  data: SliderThemeData(
                      activeTrackColor: appColor,
                      trackHeight: 2.5,
                      inactiveTrackColor: darkblack,
                      thumbColor: appColor,
                      overlayColor: Colors.transparent,
                      overlayShape: RoundSliderOverlayShape(overlayRadius: 0.02)),
                  child: frs.RangeSlider(
                    lowerValue: lowPrice,
                    upperValue: maxPrice,
                    max: 22000,
                    min: 100,
                    allowThumbOverlap: true,
                    onChanged: (low, max) {
                      setState(() {
                        lowPrice = low;
                        maxPrice = max;
                      });
                    },
                  ),
                )),
            Container(
                margin: EdgeInsets.only(top: size.convert(context, 20)),
                padding:
                EdgeInsets.symmetric(horizontal: size.convert(context, 13)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: size.convertWithHeight(context, 260),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            availableTasks,
                            style: TextStyle(
                                fontSize: size.convert(context, 20),
                                fontFamily: "HelveticaNeue",
                                fontWeight: FontWeight.w600,
                                color: Colors.black87),
                          ),
                          Text(
                            "Hide tasks those are already assigned or completed",
                            style: TextStyle(
                                fontSize: size.convert(context, 10),
                                fontFamily: "HelveticaNeue",
                                fontWeight: FontWeight.w600,
                                color: Colors.black87),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: size.convertWithHeight(context, 42),
                      child: _toggleButton(),)
                  ],
                )),
            Container(
                margin: EdgeInsets.only(top: size.convert(context, 30)),
                child: GestureDetector(
                    onTap: () {
                      setState(() {
                        serachdetails = false;
                        getPost(serachData: true);
                      });
                    },
                    child: CustomButton(
                      txt: apply,
                      hight: size.convert(context, 60),
                      filledColor: appColor,
                    ))),
            // Container(child:RangeSliderData(),),
          ],
        ),
      ),
    );
  }
  changeTaskType(int index) {
    int i = 0;
    setState(() {
      tasktypeList.forEach((val) {
        if (index == i) {
          print("same $i $index");
          taskType = index+1;
          val["select"] = true;
        } else {
          val["select"] = false;
        }
        i = i + 1;
      });
    });
  }
  _toggleButton() {
    return Container(
      width: size.convert(context, 42),
      height: size.convert(context, 20),
      decoration: BoxDecoration(
          color: darkblack,
          borderRadius: BorderRadius.circular(size.convert(context, 10))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          !onlyAvailibeTask
              ? GestureDetector(
            onTap: () {
              setState(() {
                onlyAvailibeTask = !onlyAvailibeTask;
              });
            },
            child: Container(
              height: size.convert(context, 20),
              width: size.convert(context, 25),
              decoration: BoxDecoration(
                  borderRadius:
                  BorderRadius.circular(size.convert(context, 10)),
                  color: whiteColor),
            ),
          )
              : Container(
            height: 20,
            width: 15,
          ),
          onlyAvailibeTask
              ? GestureDetector(
            onTap: () {
              setState(() {
                onlyAvailibeTask = !onlyAvailibeTask;
              });
            },
            child: Container(
              height: size.convert(context, 20),
              width: size.convert(context, 25),
              decoration: BoxDecoration(
                borderRadius:
                BorderRadius.circular(size.convert(context, 10)),
                color: appColor,
              ),
            ),
          )
              : Container(
            height: 20,
            width: 15,
          ),
        ],
      ),
    );
  }
  selectlatilng(String placeId) async {
    _placesDetailsResponse = await _places.getDetailsByPlaceId(placeId);
    _vicinity = _placesDetailsResponse.result.vicinity;

  }
  @mustCallSuper
  void onResponseError(PlacesAutocompleteResponse res) {
    if (!mounted) return;

    if (onError != null) {
      onError(res);
      print("ERROR $res");
    }
    print("success ${res.errorMessage} ${res.status}");

    if (!mounted) return;
    setState(() {
      _autocompleteResponse = null;
      //_searching = false;
    });
  }

  @mustCallSuper
  void onResponse(PlacesAutocompleteResponse res) {
    if (!mounted) return;
    print("success 2 $res");

    if (!mounted) return;
    setState(() {
      _autocompleteResponse = res;
      //_searching = false;
    });

    if (_autocompleteResponse != null)
      _autocompleteResponse.predictions.forEach((pre) {
//        print(" Des ${pre.placeId} ${pre.description} ${pre.reference}"
//            " ${pre.types.toString()} "
//            " Len ${pre.matchedSubstrings.length} ${pre.terms.length} "
//            "Main ${pre.structuredFormatting.mainText} "
//            "Secondary ${pre.structuredFormatting.secondaryText} ");
      });
  }

  Future<Null> doSearch(String value) async {
    if (mounted && value.isNotEmpty) {
      if (!mounted) return;
      setState(() {
        //_searching = true;
      });
      var res;
      try {
        res = await _places.autocomplete(
          value,
          language: "en",
          components: [Component(Component.country, "in")],
        );
      }
      catch (e) {
        return;
      }

      if (res.errorMessage?.isNotEmpty == true ||
          res.status == "REQUEST_DENIED") {
        onResponseError(res);
      } else {
        onResponse(res);
      }
    } else {
      onResponse(null);
    }
  }
  getPost({Map<dynamic, dynamic> data,bool serachData = false}) async {
    task.clear();
    markList.clear();
    String url = api["taskooApi"]["mainUrl"] + api["taskooApi"]["postTask"];
      if(onlyAvailibeTask){
        url =  url+"?ts=2&d=${distance.toInt()}&minp=${lowPrice.toInt()}&maxp=${maxPrice.toInt()}&l=${_vicinity}&tt=${taskType}";
      }
      else{
        url =  url+"?ts=&d=${distance.toInt()}&minp=${lowPrice.toInt()}&maxp=${maxPrice.toInt()}&l=${_vicinity}&tt=${taskType}";
      }
    try {
      if (!mounted) return;
      setState(() {
        loading = true;
      });
      print(url);
      var res = await _serverRequest.getMethod(url);
      print(res);
      if (res != null) {
         if(res is Response) {
          res.data.forEach((d) {
            task.add(PostTask.fromJson(d));
          });
          if(task.length>=1){
            setData(isserach: true);
            setState(() {
              loading = false;
            });
            return;
          }
          else{
            setState(() {
              loading = false;
            });
            CustomSnackBar.SnackBar_3Success(context, _scaffold,
                title: "No Result");
            return;
          }
        }
        if (!mounted) return;
        setState(() {
          loading = false;
        });
      }
      else if (res is SocketException) {
        setState(() {
          loading = false;
        });
        CustomSnackBar.SnackBar_3Success(context, _scaffold,
            title: internetIssue);
        return;
      }
      else{
        setState(() {
          loading = false;
        });
        CustomSnackBar.SnackBar_3Success(context, _scaffold,
            title: serverError);
        return;
      }
    } catch (e) {
      print("error in = " + e.toString());
      setState(() {
        loading = false;
      });
      CustomSnackBar.SnackBar_3Success(context, _scaffold,
          title: serverError);
    }
  }

}