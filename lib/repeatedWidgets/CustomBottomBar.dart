import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:page_transition/page_transition.dart';
import 'package:taskoo/pages/browserTasks.dart';
import 'package:taskoo/pages/myTasks.dart';
import 'package:taskoo/pages/notifications.dart';
import 'package:taskoo/pages/profile.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/size.dart';
import 'package:taskoo/res/string.dart';
import 'package:taskoo/res/style.dart';

import '../main.dart';
class CustomBottomBar extends StatefulWidget {
  int select ;
  CustomBottomBar(this.select);
  @override
  _CustomBottomBarState createState() => _CustomBottomBarState();
}

class _CustomBottomBarState extends State<CustomBottomBar> {

  @override
  Widget build(BuildContext context) {
    double h = size.convert(context, 24);
    double w = size.convert(context, 24);

    return Container(
      padding: EdgeInsets.symmetric(horizontal: size.convert(context, 10)),
      decoration:BoxDecoration(
        color: Color(0xffE0E0E0),
      ),
      height: size.convert(context, 70),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: (){
              print("1");
              setState(() {
                widget.select = 1;
                Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: Home()));
              });
            },
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  widget.select == 1 ? SvgPicture.asset("assets/svg_Icons/post-h.svg",
                  width: w,
                  height: h,):SvgPicture.asset("assets/svg_Icons/post.svg",
                    width: w,
                    height: h,),
//
                  SizedBox(height: 5,),
                  Text(postATask,
                    style: widget.select == 1 ? bottomSelectTextstyle: bottomUnselectTextstyle ,)
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: (){
              setState(() {
                widget.select = 2;
                Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: MyTasks()));
              });
            },
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  widget.select == 2 ? SvgPicture.asset("assets/svg_Icons/brows-h.svg",
                    width: w,
                    height: h,):SvgPicture.asset("assets/svg_Icons/browse.svg",
                    width: w,
                    height: h,),
                  SizedBox(height: size.convert(context, 5),),
                  Text(myTask,
                    style: widget.select == 2 ? bottomSelectTextstyle: bottomUnselectTextstyle ,)
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: (){
              setState(() {
                widget.select = 3;
                Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: browserTasks()));
              });
            },
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  widget.select == 3 ? SvgPicture.asset("assets/svg_Icons/searc-h.svg",
                    width: w,
                    height: h,):SvgPicture.asset("assets/svg_Icons/search.svg",
                    width: w,
                    height: h,),
                  SizedBox(height: size.convert(context, 5),),
                  Text(browseTasks,
                    style: widget.select == 3 ? bottomSelectTextstyle: bottomUnselectTextstyle ,)
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: (){
              setState(() {
                widget.select = 4;
                Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: notifications( )));
              });

            },
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  widget.select == 4 ? SvgPicture.asset("assets/svg_Icons/notification-h.svg",
                    width: w,
                    height: h,):SvgPicture.asset("assets/svg_Icons/notification.svg",
                    width: w,
                    height: h,),
                  SizedBox(height: size.convert(context, 5),),
                  Text(notification,
                    style: widget.select == 4 ? bottomSelectTextstyle: bottomUnselectTextstyle ,)
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: (){
              setState(() {
                widget.select = 5;
                Navigator.pushReplacement(context, PageTransition(type: PageTransitionType.fade, child: profile()));
              });

            },
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  widget.select == 5 ? SvgPicture.asset("assets/svg_Icons/user-h.svg",
                    width: w,
                    height: h,):SvgPicture.asset("assets/svg_Icons/user.svg",
                    width: w,
                    height: h,),
                  SizedBox(height: 5,),
                  Text(txtProfile,
                    style: widget.select == 5 ? bottomSelectTextstyle: bottomUnselectTextstyle ,)
                ],
              ),
            ),
          ),

        ],
      ),
    );
  }
}
