import 'package:flutter/material.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/size.dart';
class CutomContainerTime extends StatelessWidget {
  Widget WIcon;
  String txt1;
  String txt2;
  Color txtColor;
  Color selectColor ;
  double height;
  double width;
  CutomContainerTime({this.txtColor,this.width,this.height,this.selectColor,this.txt1, this.txt2, this.WIcon});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height == null ? size.convert(context, 135) : height,
      width: width ==null ? size.convertWithWidth(context, 190): width,
      decoration: BoxDecoration(
        color: selectColor ==null ? whiteColor: selectColor,
        borderRadius: BorderRadius.circular(size.convert(context, 10)),
        border: Border.all(
          color: darkblack,
          width: 1,
        )
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(child: WIcon == null ? Container():WIcon,),
          SizedBox(height: size.convert(context, 15),),
          Container(child: Text(txt1 == null ? "" : txt1,
          style: TextStyle(
           // fontWeight: FontWeight.w600,
          //  fontFamily: "HelveticaNeue",
            fontSize: size.convert(context, 18),
            color: txtColor??Colors.black,
          ),),),
          Container(child: Text(txt2 == null ? "" : txt2,
            style: TextStyle(
           //   fontWeight: FontWeight.w600,
              fontFamily: "HelveticaNeue",
              fontSize: size.convert(context, 12),
              color: txtColor??darkblack,
            ),),),
        ],
      ),
    );
  }
}
