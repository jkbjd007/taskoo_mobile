import 'package:flutter/material.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/size.dart';
class signUpButton extends StatelessWidget {
  double h;
  double w;
  Color buttonColor;
  String buttonText;
  signUpButton({this.buttonColor, this.buttonText, this.w, this.h});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: w?? size.convert(context, 274),
      height: h?? size.convert(context, 50),
      decoration: BoxDecoration(
        color: buttonColor??appColor,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Center(child: Text(buttonText??"Empty",style: TextStyle(
        fontSize:  20,
        fontFamily: "HelveticaNeue",
        color: whiteColor,
      ),),),
    );
  }
}
