
class menu {
  List<MenaList> menuList;

  menu({this.menuList});

  menu.fromJson(Map<String, dynamic> json) {
    if (json['menaList'] != null) {
      menuList = new List<MenaList>();
      json['menaList'].forEach((v) {
        menuList.add(new MenaList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.menuList != null) {
      data['menaList'] = this.menuList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MenaList {
  String name;
  String iconurl;

  MenaList({this.name, this.iconurl});

  MenaList.fromJson(Map<String, dynamic> json) {
    name = json['Name'];
    iconurl = json['iconurl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Name'] = this.name;
    data['iconurl'] = this.iconurl;
    return data;
  }
}