class Budget {
  String  type;
  int amount;
  int hours;

  Budget({this.type, this.amount, this.hours});

  Budget.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    amount = json['amount'];
    hours = json['hours'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['amount'] = this.amount;
    data['hours'] = this.hours;
    return data;
  }
}