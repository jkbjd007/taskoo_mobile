import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:page_transition/page_transition.dart';
import 'package:taskoo/main.dart';
import 'package:taskoo/model/otpResponse.dart';
import 'package:taskoo/pages/setUserDate.dart';
import 'package:taskoo/repeatedWidgets/CustomAppBar.dart';
import 'package:taskoo/repeatedWidgets/googleButton.dart';
import 'package:taskoo/repeatedWidgets/otpTextField.dart';
import 'package:taskoo/repeatedWidgets/signButton.dart';
import 'package:taskoo/repeatedWidgets/snackbar.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/serverCall.dart';
import 'package:taskoo/res/string.dart';
import 'package:taskoo/res/style.dart';
import 'package:taskoo/res/size.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
class signUp extends StatefulWidget {
  @override
  _signUpState createState() => _signUpState();
}

class _signUpState extends State<signUp> {
  serverRequest _serverRequest = serverRequest();
  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  bool loading = true;
  TextEditingController _controllerOtp = TextEditingController();
  TextEditingController _controllerPhoneNumber = TextEditingController();
  bool isNumberScreen = true;
  bool isNumberEmpty = true;
  bool isOtpEmpty = true;
  var api;

//  final FirebaseAuth _firebaseAuthauth = FirebaseAuth.instance;
//  final GoogleSignIn googleSignIn = new GoogleSignIn();

   _signIn() async {
//    try{
//      GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
//      GoogleSignInAuthentication googleAuth = await googleSignInAccount.authentication;
//
//      AuthCredential credential = GoogleAuthProvider.getCredential(
//        accessToken: googleAuth.accessToken,
//        idToken: googleAuth.idToken,
//      );
//      FirebaseUser user = (await _firebaseAuthauth.signInWithCredential(credential)).user;
//      print("User Name : ${user.displayName}");
//      print("Email : ${user.email}");
//      print("photoUrl : ${user.photoUrl}");
//      print("photoUrl : ${user.phoneNumber}");
//      print("photoUrl : ${user.uid}");
//    }
//    catch(e){
//      print("Gmail Login Error : "+e.toString());
//    }
    //return user;
  }

//  void _signOut() {
//    googleSignIn.signOut();
//    print("User Signed out");
//  }
  readfile() async {
    String data = await rootBundle
        .loadString("assets/propertyfile/EndPointApi.json")
        .whenComplete(() {
      setState(() {
        loading = false;
      });
    });
    api = jsonDecode(data);
    print(api);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    readfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 100)),
        child: CustomAppBarSingleColor(
          color1: whiteColor,
          parentContext: context,
          centerWigets: Text(
            welcomeFamily,
            style: appBarStyle,
          ),
          hight: size.convert(context, 100),
        ),
      ),
      body: Container(
        child: loading
            ? Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: size.convert(context, 100),
                height: size.convert(context, 100),
                child: LoadingIndicator(
                  color: appColor,
                  indicatorType: Indicator.ballScale,
                ),
              ),
            ],
          ),
        )
            : _body(),
      ),

    );
  }

  _body() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: whiteColor,
      child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
               Container(
                 child: Column(children: <Widget>[
                   SizedBox(height: size.convert(context, 100),),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: <Widget>[
                       Text("Login or Register",
                         style: TextStyle(
                             fontFamily: "HelveticaNeue",
                             fontSize:  18,
                             color: Colors.black
                         ),),
                     ],
                   ),
                   SizedBox(height: size.convert(context, 10),),
                   GestureDetector(
                     onTap: (){
                       _signIn();
                     },
                     child: loginButton(
                       height: size.convert(context, 50),
                       width: size.convert(context, 274),
                       selectColor: Colors.transparent,
                       WIcon: Center(child: Image.asset("assets/icons/1280px-Google_2015_logo.png"),),
                     ),
                   ),
                   SizedBox(height: size.convert(context, 30),),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: <Widget>[
                       Text("Login or Register",
                         style: TextStyle(
                             fontFamily: "HelveticaNeue",
                             fontSize:  18,
                             color: Colors.black
                         ),),
                     ],
                   ),
                   SizedBox(height: size.convert(context, 10),),
                   isNumberScreen? otpTextField(
                       hints: "Enter mobile number",
                       textEditingController: _controllerPhoneNumber,
                       onchange: (val){
                         if(val.toString().length>5){
                           setState(() {
                             isNumberEmpty = false;
                           });
                         }
                         else{
                           setState(() {
                             isNumberEmpty = true;
                           });
                         }
                       },
                     ):otpTextField(hints: "Enter otp",
                     textEditingController: _controllerOtp,
                     onchange: (val){
                       if(val.toString().length>3){
                         setState(() {
                           isOtpEmpty = false;
                         });
                       }
                       else{
                         setState(() {
                           isOtpEmpty = true;
                         });
                       }
                     },
                   ),
                   SizedBox(height: size.convert(context, 10),),
                   isNumberScreen?GestureDetector(
                     onTap: (){
                       if(!isNumberEmpty){
//                         getToken(false);
                       setState(() {
                         isNumberScreen = false;
                       });
                       }
                     },
                     child: signUpButton(
                       buttonText : "Send otp",
                       buttonColor: isNumberEmpty ? darkblack: appColor,
                     ),
                   ):GestureDetector(
                     onTap: (){
                       if(!isOtpEmpty){
                         Navigator.push(context, PageTransition(child: Home(),type: PageTransitionType.fade));
                       }
                     },
                     child: signUpButton(
                       buttonText : "Verify otp ",
                       buttonColor: isOtpEmpty ? darkblack: appColor,
                     ),
                   ),
                 ],),
               ),
                SizedBox(height: size.convert(context, 200),),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      margin: EdgeInsets.symmetric(horizontal: size.convert(context, 20)),
                      child: RichText(
                        text: TextSpan(children: [
                          TextSpan(text: "By login or register you are agreeing to the"
                            ,style: labelStyle,),
                          TextSpan(text: " terms and conditions"
                            ,style: labelBlueUnderLineStyle,),
                          TextSpan(text: " and "
                            ,style: labelStyle,),
                          TextSpan(text: "privacy polic"
                            ,style: labelBlueUnderLineStyle,),
                          TextSpan(text: " of taskoo"
                            ,style: labelStyle,),

                        ]),
                      ),),
                ),

              ],
            ),
          ),
    );
  }
//  getToken(bool verifyOtp,{Map<dynamic, dynamic> data}) async {
//    String url;
//    if(verifyOtp) {
//      url = api["taskooApi"]["mainUrlauth"] + api["taskooApi"]["verify-otp"] +"?phone=${_controllerPhoneNumber.text}&otp=${_controllerOtp.text}";
//    }
//      else{
//      url = api["taskooApi"]["mainUrlauth"] + api["taskooApi"]["send-otp"]+"?phone=${_controllerPhoneNumber.text}";
//      }
//    try {
//      print("call to get data");
//      if (!mounted) return;
//      setState(() {
//        loading = true;
//      });
//      print(url);
//      var res = await _serverRequest.getMethod(url);
//      print(res);
//      if (res != null) {
//        if (res is Response) {
//          if(res.statusCode<201){
//            if(!verifyOtp){
//              CustomSnackBar.SnackBar_3Success(context, _scaffold,
//                  title: res.toString());
//              Future.delayed(const Duration(seconds: 1), () {
//                setState(() {
//                  isNumberScreen = false;
//                });
//              });
//            }
//            if(verifyOtp){
//              otpResponse _otpResponse = otpResponse.fromJson(res.data);
//              authModel.setPrefs_Authenticate(id:_otpResponse.user.id??1,email: _otpResponse.user.email??"",
//              imageUrl: _otpResponse.user.imageUrl??"",name: _otpResponse.user.name??"",phone: _otpResponse.user.phone??"",
//              userId: _otpResponse.user.userId??"",token: _otpResponse.token??"",islogin: true);
//              Navigator.push(context, PageTransition(child: Home(),type: PageTransitionType.fade));
//            }
//
//          }
//        }
//        if (!mounted) return;
//        setState(() {
//          loading = false;
//        });
//      }
//      if (res is SocketException) {
//        setState(() {
//          loading = false;
//        });
//        CustomSnackBar.SnackBar_3Success(context, _scaffold,
//            title: internetIssue);
//      }
//    } catch (e) {
//      setState(() {
//        loading = false;
//      });
//      print("error in = " + e.toString());
//      CustomSnackBar.SnackBar_3Success(context, _scaffold,
//          title: serverError);
//    }
//  }
}
