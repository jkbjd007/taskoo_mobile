import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:page_transition/page_transition.dart';
import 'package:taskoo/model/user.dart';
import 'package:taskoo/pages/browserTasks.dart';
import 'package:taskoo/pages/setUserDate.dart';
import 'package:taskoo/repeatedWidgets/CustomAppBar.dart';
import 'package:taskoo/repeatedWidgets/CustomButton.dart';
import 'package:taskoo/repeatedWidgets/CustomTextField.dart';
import 'package:taskoo/repeatedWidgets/CutomContainerTime.dart';
import 'package:taskoo/repeatedWidgets/snackbar.dart';
import 'package:taskoo/repeatedWidgets/textfieldPrice.dart';
import 'package:taskoo/res/serverCall.dart';
import 'package:taskoo/res/size.dart';
import 'package:taskoo/res/color.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:rxdart/rxdart.dart';
import 'package:date_format/date_format.dart';
import 'package:taskoo/res/string.dart';
import 'package:taskoo/res/style.dart';

class postTask extends StatefulWidget {
  @override
  _postTaskState createState() => _postTaskState();
}

class _postTaskState extends State<postTask>
    with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formkey1 = GlobalKey<FormState>();
  GlobalKey<FormState> _formkey2 = GlobalKey<FormState>();
  GlobalKey<FormState> _formkey3 = GlobalKey<FormState>();
  serverRequest _serverRequest = serverRequest();
  PlacesDetailsResponse _placesDetailsResponse;
  final _queryBehavior = BehaviorSubject<String>.seeded('');
  userData currentUser = userData();
  String token = "";
  bool _searching = true;
  bool dropdown = false;
  bool isTitleEmpty = true;
  bool isdesEmpty = true;
  bool isVicintyEmpty = true;
  bool isDateEmpty = true;
  bool timeSlotError = false;
  bool imageError = false;
  bool ishourEmpty = true;
  bool ishourPriceEmpty = true;
  bool istotalPriceEmpty = true;
  int charCount = 20;
  bool form1buttonEnable = false;
  bool form2ButtonEnable = false;
  bool form3ButtonEnable = false;
  int minCount = 20;
  ///these varible for to pass in post task
  int tasktype = 2;/// 1: Inperson 2: remote
  int dueTime = 1; /// 1: Morning 2: Moon 3: After Morning 4: Evening
  String latitudeSelected = "";
  String longitudeSelected = "";
  String locationName = "";
  String placeId = "";
  String vicinity = "";
  String category = "laundary services";
  DateTime _selectedDate = DateTime(2000, 01, 01);
  int budgetType = 1;/// 1 for total and 2 for hourly
  List file = [];

  ///these varible for to pass in post task
  List timedslot = [
    {
      "slot": "Morning",
      "time": "Before 10am",
      "Icon": "assets/svg_Icons/morning.svg",
      "Icon-w":"assets/svg_Icons/morning-w.svg",
      "select": true,
    },
    {
      "slot": "Noon",
      "time": "10am to 12pm",
      "Icon": "assets/svg_Icons/noon.svg",
      "Icon-w":"assets/svg_Icons/noon-w.svg",
      "select": false,
    },
    {
      "slot": "Afternoon",
      "time": "2pm to 6am",
      "Icon": "assets/svg_Icons/aternoon.svg",
      "Icon-w":"assets/svg_Icons/aternoon-w.svg",
      "select": false,
    },
    {
      "slot": "Evening",
      "time": "After 6am",
      "Icon": "assets/svg_Icons/evening.svg",
      "Icon-w":"assets/svg_Icons/evening-w.svg",
      "select": false,
    },
  ];

  int selectedPage = 0;
  bool isremate = true;

  bool daytimeselected = true;
  bool total = true;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );
  GoogleMapsPlaces _places;
  PlacesAutocompleteResponse _autocompleteResponse;
  TextEditingController _localityController = TextEditingController();
  TextEditingController _titleController = TextEditingController();
  TextEditingController _desController = TextEditingController();
  TextEditingController _hourController = TextEditingController();
  TextEditingController _hourPriceController = TextEditingController();
  TextEditingController _totalPriceController = TextEditingController();

  ValueChanged<PlacesAutocompleteResponse> onError;
  var api ;
  bool loading = true;
  readfile() async {
    String data = await rootBundle.loadString("assets/propertyfile/EndPointApi.json").whenComplete((){
      setState(() {
        loading = false;
      });
    });
    api = jsonDecode(data);
    print(api);

  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      selectedPage = 0;
    });
    readfile();
    _places = GoogleMapsPlaces(apiKey: Googlekey);

    _queryBehavior.stream
        .debounceTime(const Duration(milliseconds: 300))
        .listen(doSearch);
    _localityController.addListener(() {
      _queryBehavior.add(_localityController.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 100)),
        child: CustomAppBarSingleColor(
          parentContext: context,
          centerWigets: Text(
            postATask,
            style: appBarStyle,
          ),
          leadingIcon: selectedPage == 0
              ? GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.clear,
                    color: darkblack,
                    size: size.convert(context, 28),
                  ),
                )
              : GestureDetector(
                  onTap: () {
                    if (selectedPage > 0) {
                      setState(() {
                        selectedPage = selectedPage - 1;
                        pageChanged(selectedPage);
                      });
                    }
                  },
                  child: Icon(
                    Icons.arrow_back,
                    color: darkblack,
                    size: size.convert(context, 28),
                  ),
                ),
          hight: size.convert(context, 100),
        ),
      ),
      body:
      Container(
        color: darkblack.withOpacity(0.05),
        padding: EdgeInsets.symmetric(horizontal: size.convert(context, 10)),
        child: _body(),
      ),
    );
  }

  _body() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: darkblack.withOpacity(0.05),
      child: Column(
        children: <Widget>[
          _topsearch(),
          SizedBox(
            height: size.convert(context, 10),
          ),
          _taskInfo(),
        ],
      ),
    );
  }

  _topsearch() {
    TextStyle selectedstyle = TextStyle(
      color: Colors.white,
      fontFamily: "HelveticaNeuBold",
      fontSize: size.convert(context, 12),
    );
    TextStyle unselectedstyle = TextStyle(
      color: darkblack,
      fontFamily: "HelveticaNeuBold",
      fontSize: size.convert(context, 12),
    );
    BoxDecoration selectedCont = BoxDecoration(
        borderRadius: BorderRadius.circular(size.convert(context, 10)),
        color: appColor);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: size.convertWithWidth(context, 15)),
      //height: size.convert(context, 35),
      padding: EdgeInsets.only(top: size.convert(context, 15)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              //pageChanged(0);
            },
            child: Container(
              padding: EdgeInsets.only(
                  top: size.convert(context, 2),
                  bottom: size.convert(context, 2),
                  left: size.convert(context, 10),
                  right: size.convert(context, 10)),
              child: Center(
                child: Text(
                  detail,
                  style: selectedPage == 0 ? selectedstyle : unselectedstyle,
                ),
              ),
              decoration: selectedPage == 0 ? selectedCont : BoxDecoration(),
            ),
          ),
          GestureDetector(
            onTap: () {
            },
            child: Container(
              padding: EdgeInsets.only(
                  top: size.convert(context, 2),
                  bottom: size.convert(context, 2),
                  left: size.convert(context, 10),
                  right: size.convert(context, 10)),
              child: Center(
                child: Text(
                  dateTimeText,
                  style: selectedPage == 1 ? selectedstyle : unselectedstyle,
                ),
              ),
              decoration: selectedPage == 1 ? selectedCont : BoxDecoration(),
            ),
          ),
          GestureDetector(
            onTap: () {
              //   pageChanged(2);
            },
            child: Container(
              padding: EdgeInsets.only(
                  top: size.convert(context, 2),
                  bottom: size.convert(context, 2),
                  left: size.convert(context, 10),
                  right: size.convert(context, 10)),
              child: Center(
                child: Text(
                  budgetText,
                  style: selectedPage == 2 ? selectedstyle : unselectedstyle,
                ),
              ),
              decoration: selectedPage == 2 ? selectedCont : BoxDecoration(),
            ),
          ),
        ],
      ),
    );
  }

  _taskInfo() {
    return Expanded(
      child: Container(
        child: PageView(
          controller: pageController,
          physics: NeverScrollableScrollPhysics(),
//          onPageChanged: (index) {
//            pageChanged(index);
//          },
          children: <Widget>[
            details(),
            dateTime(),
            budget(),
          ],
          //physics: NeverScrollableScrollPhysics(),
        ),
      ),
    );
  }

  void pageChanged(int index) {
    print(index);

    setState(() {
      //selectedPage = index + 1;
      pageController.animateToPage(selectedPage,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  Widget details() {
    return Container(
      child: SingleChildScrollView(
        child: Form(
          key: _formkey1,
          child: Column(
            children: <Widget>[
              Container(
                padding:
                    EdgeInsets.symmetric(horizontal: size.convert(context, 5)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: (charCount <= 0)
                          ? Container()
                          : Text(
                              "$charCount $charLiftText",
                              style: TextStyle(
                                color: appColor,
                                fontFamily: "HelveticaNeue",
                                fontSize: size.convert(context, 10),
                                //fontWeight: FontWeight.w600,
                              ),
                            ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: size.convert(context, 5),
              ),
              Container(
                child: CustomTextField(
                  color1: darkblack,
                  textEditingController: _titleController,
                  maxlength: 20,
                  hints: postTitleText,
                  borderwidth: 1,
                  onchange: (val) {
                    setState(() {
                      charCount = 20 - val.length;
                    }); if (val.toString().length > 20) {
                      setState(() {
                        isTitleEmpty = false;
                      });
                      detailFormValidation();
                    } else {
                      setState(() {
                        isTitleEmpty = true;
                      });
                      detailFormValidation();
                    }
                    print(" $charCount");
                  },
                ),
              ),
              SizedBox(
                height: size.convert(context, 12),
              ),
              Container(
                child: CustomTextField(
                  color1: darkblack,
                  textEditingController: _desController,
                  hints: postdesText,
                  borderwidth: 1,
                  contHight: size.convert(context, 252),
                  maxline: 10,
                  onchange: (val){
                    if (val.toString().length > 0) {
                      setState(() {
                        isdesEmpty = false;
                      });
                      detailFormValidation();
                    } else {
                      setState(() {
                        isdesEmpty = true;
                      });
                      detailFormValidation();
                    }
                  },
                ),
              ),
              SizedBox(
                height: size.convert(context, 12),
              ),
              GestureDetector(
                onTap: () {
                },
                child: CustomContainer(
                  txt: attachmentsText,
                  contHight: size.convert(context, 50),
                  widgetIcon: Container(
                    child: Image.asset(
                      "assets/icons/Icon feather-file-plus.png",
                      height: size.convert(context, 20),
                      width: size.convert(context, 16),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: size.convert(context, 10),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: size.convertWithHeight(context, 290),
                      child: Text(
                        remoteTaskDoneText,
                        style: hintsLineText20Style,
                      ),
                    ),
                     Container(
                        width: size.convertWithHeight(context, 42),
                        child: _toggleButton(),
                      ),
                  ],
                ),
              ),
              SizedBox(
                height: size.convert(context, 10),
              ),
              Container(
                child: CustomTextField(
                  color1: darkblack,
                  onchange: (val){
                    if (val.toString().length > 0) {
                      setState(() {
                        isVicintyEmpty = false;
                      });
                      detailFormValidation();
                    } else {
                      setState(() {
                        isVicintyEmpty = true;
                      });
                      detailFormValidation();
                    }
                  },
                  textEditingController: _localityController,
                  ontap: () async {
                    setState(() {
                      dropdown = true;
                    });
                  },
                  onsave: () {
                    setState(() {
                      dropdown = false;
                    });
                  },
                  enable: isremate,
                  hints: isremate
                      ? localityHints
                      : disableLocalityHints,
                  trailingIcon: isremate ?SvgPicture.asset("assets/svg_Icons/searc-h.svg",):SvgPicture.asset("assets/svg_Icons/search.svg",),
                  borderwidth: 1,
                ),
              ),
              dropdown
                  ? AnimatedContainer(
                      margin: EdgeInsets.symmetric(
                          horizontal: size.convert(context, 5)),
                      padding: EdgeInsets.only(top: size.convert(context, 20)),
                      color: whiteColor,
                      duration: Duration(microseconds: 3),
                      child: _autocompleteResponse == null
                          ? SizedBox()
                          : ListView.separated(
                              physics: ScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectlatilng(_autocompleteResponse
                                          .predictions[index].placeId);
                                      _localityController.text =
                                          _autocompleteResponse
                                              .predictions[index]
                                              .structuredFormatting
                                              .mainText;
                                      _autocompleteResponse = null;
                                      dropdown = false;
                                    });
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: size.convert(context, 10)),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Icon(
                                          IcoFontIcons.locationPin,
                                          color: darkblack,
                                          size: size.convert(context, 20),
                                        ),
                                        SizedBox(
                                          width: size.convert(context, 1),
                                        ),
                                        Expanded(
                                          child: Text(
                                            _autocompleteResponse
                                                .predictions[index]
                                                .structuredFormatting
                                                .mainText,
                                            style: TextStyle(
                                                fontSize:
                                                    size.convert(context, 20),
                                                fontFamily: "HelveticaNeue",
                                                color: darkblack,
                                            //    fontWeight: FontWeight.w600
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return SizedBox(
                                  height: 12,
                                );
                              },
                              itemCount:
                                  _autocompleteResponse.predictions.length),
                    )
                  : SizedBox(),
              SizedBox(
                height: size.convert(context, 10),
              ),
              GestureDetector(
                  onTap: () {
                      if (form1buttonEnable) {
                        _getUserData();
                        if (selectedPage < 2) {
                          setState(() {
                            selectedPage = selectedPage + 1;
                            pageChanged(selectedPage);
                            if (isremate) {
                              tasktype = 2;
                            } else {
                              tasktype = 1;
                            }
                          });
                          print("title : " +
                              _titleController.text +
                              "\n" +
                              "Descrption : " +
                              _desController.text +
                              "\n" +
                              "lati : " +
                              latitudeSelected +
                              "\n" +
                              "long : " +
                              longitudeSelected +
                              "\n" +
                              "tasktype : " +
                              tasktype.toString() +
                              "\n"+
                              "Place id : " +
                              placeId.toString() +
                              "\n");
                        }
                      }

                  },
                  child: CustomButton(
                    txt: continuebuttonText,
                    filledColor: form1buttonEnable?appColor:darkblack,
                    hight: size.convert(context, 60),
                  )),
              SizedBox(
                height: size.convert(context, 10),
              )
            ],
          ),
        ),
      ),
    );
  }
  _getUserData() async {
    currentUser.id =  await authModel.getId();
    currentUser.name = await authModel.getUserName();
    currentUser.email = await authModel.getUserEmail();
    currentUser.phone = await authModel.getUserPhone();
    currentUser.userId = await authModel.getUserId();
    currentUser.imageUrl = await authModel.getUserImageUrl();
    token = await authModel.getUserToken();
    print("User Name ="+currentUser.name);
    print("User Token = "+token);
  }
  detailFormValidation() {
    print("differentBudget ${isremate}");
    print("isdesEmpty ${isTitleEmpty}");
    print("istotalPriceEmpty ${isdesEmpty}");
    if (isremate) {
      print("1111");
      if (!isdesEmpty && !isTitleEmpty && !isVicintyEmpty ) {
        setState(() {
          form1buttonEnable = true;
        });
      } else {
        setState(() {
          form1buttonEnable = false;
        });
      }
    }
    else if (!isremate) {
      print("2222");
      if (!isdesEmpty && !isTitleEmpty) {
        setState(() {
          form1buttonEnable = true;
        });
      } else {
        setState(() {
          form1buttonEnable = false;
        });
      }
    }
  }

  selectlatilng(String _placeId) async {
    _placesDetailsResponse = await _places.getDetailsByPlaceId(_placeId);
    latitudeSelected =
        _placesDetailsResponse.result.geometry.location.lat.toString();
    longitudeSelected =
        _placesDetailsResponse.result.geometry.location.lng.toString();
    placeId = _placeId;
    locationName = _placesDetailsResponse.result.name;
    vicinity = _placesDetailsResponse.result.vicinity;
    print(latitudeSelected + " - " + longitudeSelected);
  }

  @mustCallSuper
  void onResponseError(PlacesAutocompleteResponse res) {
    if (!mounted) return;

    if (onError != null) {
      onError(res);
      print("ERROR $res");
    }
    print("success ${res.errorMessage} ${res.status}");

    if (!mounted) return;
    setState(() {
      _autocompleteResponse = null;
      _searching = false;
    });
  }

  @mustCallSuper
  void onResponse(PlacesAutocompleteResponse res) {
    if (!mounted) return;
    print("success 2 $res");

    if (!mounted) return;
    setState(() {
      _autocompleteResponse = res;
      _searching = false;
    });

    if (_autocompleteResponse != null)
      _autocompleteResponse.predictions.forEach((pre) {
//        print(" Des ${pre.placeId} ${pre.description} ${pre.reference}"
//            " ${pre.types.toString()} "
//            " Len ${pre.matchedSubstrings.length} ${pre.terms.length} "
//            "Main ${pre.structuredFormatting.mainText} "
//            "Secondary ${pre.structuredFormatting.secondaryText} ");
      });
  }

  Future<Null> doSearch(String value) async {
    if (mounted && value.isNotEmpty) {
      if (!mounted) return;
      setState(() {
        _searching = true;
      });
      var res;
      try {
        res = await _places.autocomplete(
          value,
          language: "en",
          components: [Component(Component.country, "in")],
        );
      }
      catch (e) {
        return;
      }

      if (res.errorMessage?.isNotEmpty == true ||
          res.status == "REQUEST_DENIED") {
        onResponseError(res);
      } else {
        onResponse(res);
      }
    } else {
      onResponse(null);
    }
  }

  _toggleButton() {
    return Container(
      width: size.convert(context, 42),
      height: size.convert(context, 20),
      decoration: BoxDecoration(
          color: darkblack,
          borderRadius: BorderRadius.circular(size.convert(context, 10))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          !isremate
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      isremate = !isremate;
                      if(isremate){
                        if(_localityController.text == "" ){
                          form1buttonEnable = false;
                        }
                      }
                    });
                  },
                  child: Container(
                    height: size.convert(context, 20),
                    width: size.convert(context, 25),
                    decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(size.convert(context, 10)),
                        color: whiteColor),
                  ),
                )
              : Container(
                  height: 20,
                  width: 15,
                ),
          isremate
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      isremate = !isremate;
                      _localityController.text="";
                      if(_desController.text!="" && _titleController.text!=""){
                        form1buttonEnable = true;
                      }
                    });
                  },
                  child: Container(
                    height: size.convert(context, 20),
                    width: size.convert(context, 25),
                    decoration: BoxDecoration(
                        borderRadius:
                        BorderRadius.circular(size.convert(context, 10)),
                        color: appColor),
                  ),
                )
              : Container(
                  height: 20,
                  width: 15,
                ),
        ],
      ),
    );
  }

  dateTime() {
    return Container(
      child: SingleChildScrollView(
        child: Form(
          key: _formkey2,
          child: Column(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  datePicker();
                },
                child: CustomContainer(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  txt: _selectedDate == DateTime(2000, 01, 01)
                      ? selectDateText
                      : formatDate(_selectedDate, [dd, '-', mm, '-', yyyy])
                          .toString()
                          .substring(0, 10),
                  widgetIcon: Icon(Icons.keyboard_arrow_down),
                  contHight: size.convert(context, 50),
                  leadingIcon: Container(
                    child: SvgPicture.asset(
                      "assets/icons/Icon awesome-calendar-alt.svg",
                      height: size.convert(context, 25),
                      width: size.convert(context, 21),
                    ),
                    margin: EdgeInsets.symmetric(
                        horizontal: size.convert(context, 10)),
                  ),
                ),
              ),
              SizedBox(
                height: size.convert(context, 15),
              ),
              Container(
                margin:
                    EdgeInsets.symmetric(horizontal: size.convert(context, 10)),
                child: Row(
                  children: <Widget>[
                    _checkBox(),
                    SizedBox(
                      width: size.convert(context, 10),
                    ),
                    Expanded(
                      child: Container(
                        child: RichText(
                          maxLines: 1,
                          text: TextSpan(
                              text: selectSpecficTimeText,
                              style: hintsLineText16Style),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: size.convert(context, 15),
              ),
              Container(
                margin:
                    EdgeInsets.symmetric(horizontal: size.convert(context, 5)),
                child: Row(
                  children: <Widget>[
                    RichText(
                      maxLines: 1,
                      text: TextSpan(
                          text: atWhatTimeText,
                          style: hintsLineText20Style),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: size.convert(context, 15),
              ),
              Container(
                child: StaggeredGridView.countBuilder(
                  crossAxisCount: 2,
                  itemCount: timedslot.length,
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      child: GestureDetector(
                        onTap: () {
                          changeSlot(index);
                        },
                        child: Stack(
                          children: <Widget>[
                            CutomContainerTime(
                              WIcon: Container(
                                child: SvgPicture.asset(
                                  daytimeselected
                                      ? timedslot[index]["select"]
                                      ? timedslot[index]["Icon-w"] //for Selected
                                      : timedslot[index]["Icon"]
                                      : timedslot[index]["Icon"],
                                  width: size.convert(context, 20),
                                  height: size.convert(context, 20),
                                ),
                              ),
                              txt1: timedslot[index]["slot"],
                              txt2: timedslot[index]["time"],
                              selectColor: daytimeselected
                                  ? timedslot[index]["select"]
                                      ? appColor
                                      : whiteColor
                                  : whiteColor,
                              txtColor: daytimeselected
                                  ? timedslot[index]["select"]
                                  ? whiteColor
                                  : null
                                  : null,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
                  mainAxisSpacing: size.convert(context, 7),
                  crossAxisSpacing: size.convert(context, 7),
                ),
              ),
              Container(
                margin:
                    EdgeInsets.symmetric(horizontal: size.convert(context, 10)),
                child: Row(
                  children: <Widget>[
                    SvgPicture.asset(
                      "assets/icons/Icon material-info-outline.svg",
                      height: size.convert(context, 12),
                      width: size.convert(context, 12),
                    ),
                    SizedBox(
                      width: size.convert(context, 6),
                    ),
                    Expanded(
                      child: Text(
                        sortTaskWithTimeText,
                        style: dark12Textstyle,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: size.convert(context, 50),
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: GestureDetector(
                      onTap: () {
                        if (!isDateEmpty) {
                          if (selectedPage < 2) {
                            if(!mounted)return;
                            setState(() {
                              selectedPage = selectedPage + 1;
                              pageChanged(selectedPage);
                            });
                          }
                        }
                      },
                      child: CustomButton(
                        txt: continuebuttonText,
                        hight: size.convert(context, 60),
                        filledColor: isDateEmpty? darkblack : appColor,
                      ))),
            ],
          ),
        ),
      ),
    );
  }

  datePicker() async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: Colors.transparent,
            child: Container(
              height: size.convert(context, 260),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.circular(size.convert(context, 10))),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            top: size.convert(context, 10),
                            left: size.convert(context, 20)),
                        child: Text(
                          selectDateText,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: size.convert(context, 20)),
                        ),
                      ),
                    ],
                  ),
                  DatePickerWidget(
                    firstDate: DateTime.now(),
                    lastDate: DateTime(2030, 1, 1),
                    initialDate: DateTime(2020),
                    dateFormat: "dd-MMMM-yyyy",
                    onChange: datepick,
                    pickerTheme: DateTimePickerTheme(
                        itemTextStyle: TextStyle(
                            color: Colors.black,
                            fontSize: 19,
                            fontFamily: "HelveticaNeueMedium"),
                        backgroundColor: Colors.transparent),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                          print("ok");
                          if(!mounted)return;
                          setState(() {
                            isDateEmpty = false;
                            if(_selectedDate==DateTime(2000, 01, 01)){
                              _selectedDate=DateTime.now();
                            }
                            datepick;
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.only(
                              top: size.convert(context, 10),
                              right: size.convert(context, 20)),
                          child: Text(
                            okText,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: size.convert(context, 15)),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          print("Cancel");
                          if(!mounted)return;
                          setState(() {
                            //_selectedDate = DateTime(2000,01,01);
                          });
                          Navigator.pop(context);
                        },
                        child: Container(
                          padding: EdgeInsets.only(
                              top: size.convert(context, 10),
                              right: size.convert(context, 20)),
                          child: Text(
                            cancelText,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: size.convert(context, 15)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        });
  }

  datepick(DateTime newDate, _) {
    setState(() {
      _selectedDate = newDate;
    });
  }

  changeSlot(int index) {
    int i = 0;
    if(!mounted)return;
    setState(() {
      timedslot.forEach((val) {
        if (index == i) {
          val["select"] = true;
          dueTime = i + 1;
        } else {
          val["select"] = false;
        }
        i = i + 1;
      });
    });
  }

  _checkBox() {
    return GestureDetector(
      onTap: () {
        if(!mounted)return;
        setState(() {
          daytimeselected = !daytimeselected;
        });
      },
      child: Container(
        width: size.convert(context, 25),
        height: size.convert(context, 25),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(size.convert(context, 3)),
          color: daytimeselected ? appColor : whiteColor,
        ),
        child: Center(
          child: Icon(
            Icons.done,
            color: whiteColor,
          ),
        ),
      ),
    );
  }

  budget() {
    return loading ?
    Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: size.convert(context, 100),
            height: size.convert(context, 100),
            child: LoadingIndicator(
              color: appColor,
              indicatorType: Indicator.ballScale,
            ),),
        ],
      ),):Container(
      child: SingleChildScrollView(
        child: Form(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: size.convert(context, 15),
              ),
              _switchButton(),
              SizedBox(
                height: size.convert(context, 15),
              ),
              _selectBody(),
              SizedBox(
                height: size.convert(context, 365),
              ),
              GestureDetector(
                  onTap: () {
                    if(form3ButtonEnable){
                      _postdata();
                      if (selectedPage < 2) {
                        print("page change");
                        if(!mounted)return;
                        setState(() {
                          selectedPage = selectedPage + 1;
                          pageChanged(selectedPage);
                        });
                      }
                    }
                  },
                  child: CustomButton(
                    txt: postText,
                    hight: size.convert(context, 60),
                    filledColor: form3ButtonEnable? appColor:darkblack,
                  )),
            ],
          ),
        ),
      ),
    );
  }

  _switchButton() {
    return Container(
      width: size.convertWithWidth(context, 274),
      height: size.convert(context, 40),
      decoration: BoxDecoration(
        color: darkblack,
        borderRadius: BorderRadius.circular(size.convert(context, 15)),
      ),
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              if(!mounted)return;
              setState(() {
                total = true;
                if(istotalPriceEmpty){
                  form3ButtonEnable = false;
                }
                else{
                  form3ButtonEnable = true;
                }
                budgetType = 1;
              });
            },
            child: Container(
              width: size.convertWithWidth(context, 136),
              height: size.convert(context, 40),
              decoration: BoxDecoration(
                  color: total ? appColor : darkblack,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(size.convert(context, 15)),
                    topLeft: Radius.circular(size.convert(context, 15)),
                  )),
              child: Center(
                child: Text(
                  totalText,
                  style: TextStyle(
                    color: whiteColor,
                    //fontFamily: "HelveticaNeue",
                    //fontWeight: FontWeight.w600,
                    fontSize: size.convert(context, 20),
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: size.convertWithWidth(context, 2),
            height: size.convertWithWidth(context, 40),
            color: whiteColor,
          ),
          GestureDetector(
            onTap: () {
              if(!mounted)return;
              setState(() {
                total = false;
                if(ishourEmpty || ishourPriceEmpty){
                  form3ButtonEnable = false;
                }
                else{
                  form3ButtonEnable = true;
                }
                budgetType = 2;
              });
            },
            child: Container(
              width: size.convertWithWidth(context, 136),
              height: size.convert(context, 40),
              decoration: BoxDecoration(
                color: !total ? appColor : darkblack,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(size.convert(context, 15)),
                  bottomRight: Radius.circular(size.convert(context, 15)),
                ),
              ),
              child: Center(
                child: Text(
                  hourlyText,
                  style: TextStyle(
                    color: whiteColor,
                    //fontFamily: "HelveticaNeue",
                    //fontWeight: FontWeight.w600,
                    fontSize: size.convert(context, 20),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _selectBody() {
    return Container(
      child: total
          ? Container(
              child: textfieldPrice(
                editingController: _totalPriceController,
                Cheight: size.convert(context, 50),
                Cwidth: size.convertWithWidth(context, 395),
                onchange: (val) {
                  if (val.toString().length > 0) {
                    setState(() {
                      istotalPriceEmpty = false;
                    });
                  } else {
                    setState(() {
                      istotalPriceEmpty = true;
                    });
                  }
                  _budgetFormValidation();
                },
              ),
            )
          : Container(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: CustomTextField(
                        color1: darkblack,
                        textEditingController: _hourController,
                        onchange: (val) {
                          print(val.toString().length.toString());
                          if (val.toString().length > 0) {
                            setState(() {
                              ishourEmpty = false;
                            });
                          } else {
                            setState(() {
                              ishourEmpty = true;
                            });
                          }
                          _budgetFormValidation();
                        },
                        borderwidth: 1,
                        hints: hourlyText,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: size.convert(context, 8),
                  ),
                  Container(
                    child: textfieldPrice(
                      editingController: _hourPriceController,
                      onchange: (val) {
                        print(val.toString().length.toString());
                        if (val.toString().length > 0) {
                          setState(() {
                            ishourPriceEmpty = false;
                          });
                        } else {
                          setState(() {
                            ishourPriceEmpty = true;
                          });
                        }
                        _budgetFormValidation();
                      },
                      Cwidth: size.convertWithWidth(context, 197),
                      Cheight: size.convert(context, 50),
                      borderWidth: 1,
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  _budgetFormValidation(){
    if(total){
      if(!istotalPriceEmpty){
        setState(() {
          form3ButtonEnable = true;
        });
      }
      else{
        setState(() {
          form3ButtonEnable = false;
        });
      }
    }
    else{
      if(!ishourEmpty && !ishourPriceEmpty){
        setState(() {
          form3ButtonEnable = true;
        });
      }
      else{
        setState(() {
          form3ButtonEnable = false;
        });
      }
    }
  }

  _postdata() async {
    String url = api["taskooApi"]["mainUrl"]+api["taskooApi"]["postTask"];
    print(url);
    String price = "0";
    String hour = "0";
    if (budgetType == 1) {
      price = _totalPriceController.text;
      hour = "0";
    } else {
      price = _hourPriceController.text;
      hour = _hourController.text;
    }
    print(price);
    print(hour);

    try {
      if(!mounted)return;
      setState(() {
        loading = true;
      });
      Map data = {
        "title": _titleController.text,
        "details":  _desController.text,
        "taskType": tasktype,
        "files": file,
        "mustHaves": [
          "string1",
          "string2",
          "string3"
        ],
        "location": {
          "place_id": placeId,
          "geometry": {
            "lng": latitudeSelected,
            "lat": longitudeSelected
          },
          "name": locationName,
          "vicinity": vicinity
        },
        "dueDate": formatDate(_selectedDate, [dd, '-', mm, '-', yyyy]).toString().substring(0,10),
        "dueTimeType": dueTime,
        "budget": {
          "type": budgetType,
          "amount": price,
          "hours": hour
        },
        "user": {
          "id": 1
        },
        "category": "laundary services"
      };
      //print(data);
      var res = await _serverRequest.postMethod(url,  data );
      if(res is SocketException){
        CustomSnackBar.SnackBar_3Error(context, _scaffold,
            title: internetIssue);
        print("No internet");
        if(!mounted)return;
        setState(() {
          loading = false;
          selectedPage = 2;
        });
      }
      else if(res.statusCode<202){
        CustomSnackBar.SnackBar_3Success(context, _scaffold,
            title: postTaskSuccess);
        Navigator.pushReplacement(context, PageTransition(
          child: browserTasks(),type: PageTransitionType.fade
        ));
        if(!mounted)return;
        setState(() {
          loading = false;
        });
      }
      else{
        CustomSnackBar.SnackBar_3Error(context, _scaffold,
            title: serverError);
        if(!mounted)return;
        setState(() {
          loading = false;
          selectedPage = 2;
        });
      }
    } catch (e) {
      print("error " + e.toString());
      CustomSnackBar.SnackBar_3Error(context, _scaffold,
          title: serverError);
      if(!mounted)return;
      setState(() {
        loading = false;
        selectedPage = 2;
      });
    }
  }
}
