import 'package:flutter/material.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/size.dart';
import 'package:taskoo/res/string.dart';
class textfieldPrice extends StatelessWidget {
  double Cwidth;
  double Cheight;
  double borderWidth;
  Function onchange;
  TextEditingController editingController;
  Function onvalidate;
  bool error;
  String hints;
  bool enable;
  textfieldPrice({this.enable = true ,this.onchange,this.onvalidate,this.editingController,this.error = false,this.borderWidth,this.Cheight,this.Cwidth,this.hints});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Cwidth == null ? size.convertWithWidth(context, 100) : Cwidth,
      height: Cheight == null ? size.convert(context, 40) : Cheight,
      decoration: BoxDecoration(
        color: whiteColor,
        borderRadius: BorderRadius.circular(size.convert(context, 10)),
        border:Border.all(
          width: borderWidth == null ? 1 : borderWidth,
              color: error ? Colors.red : darkblack
        )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
             // padding: EdgeInsets.symmetric(horizontal: size.convert(context, 10)),
              child: TextFormField(
                enabled: enable,
                validator: onvalidate??(val){},
                onChanged: onchange == null ? (e){} : onchange,
                controller: editingController,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: hints??"",
                border: InputBorder.none,
                hintStyle:  TextStyle(
                    fontSize: size.convert(context, 20),
                    fontFamily: "HelveticaNeue",
                    color: darkblack,
                   // fontWeight: FontWeight.w600
                ),
              ),
                style: TextStyle(
                    fontSize: size.convert(context, 20),
                    fontFamily: "HelveticaNeue",
                    color: Colors.black,
//                    fontWeight: FontWeight.w600
                ),

                cursorColor: Colors.black,
            ),
            ),
          ),
          Container(
            width: size.convertWithWidth(context, 53),
            height: size.convert(context, 50),

            decoration: BoxDecoration(
              color: darkblack.withOpacity(0.05),
              border: Border(
                left: BorderSide(width: 1,color: darkblack),
                bottom: BorderSide.none,
                top: BorderSide.none,
                right: BorderSide.none
              ),
            ),
            child: Center(child: Text(currencySign),),
          ),
        ],
      ),
    );
  }
}
