import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:taskoo/pages/browserTasks.dart';
import 'package:taskoo/pages/myTasks.dart';
import 'package:taskoo/pages/notifications.dart';
import 'package:taskoo/pages/postTask.dart';
import 'package:taskoo/pages/profile.dart';
import 'package:taskoo/pages/signUp.dart';
import 'package:taskoo/repeatedWidgets/CustomAppBar.dart';
import 'package:taskoo/repeatedWidgets/CustomBottomBar.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/size.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:taskoo/model/menu.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:taskoo/res/string.dart';
import 'package:taskoo/res/style.dart';
import 'package:taskoo/splashScreen.dart';
main(){
  runApp(startPage());
 // runApp(new MaterialApp(home: Home()));
}
class startPage extends StatefulWidget {
  @override
  _startPageState createState() => _startPageState();
}

class _startPageState extends State<startPage> {
  bool _splashScreen = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(const Duration(seconds: 5), () {
      setState(() {
        _splashScreen = false;
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: _splashScreen? splashScreen():signUp(),
    );
  }
}


class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int select = 1;
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
  menu item =menu();
  bool loading = true;
  readfile() async {
    String data = await rootBundle.loadString("assets/propertyfile/menu.json").whenComplete((){
      setState(() {
        loading = false;
      });
    });
    var menu1 = jsonDecode(data);
    item = menu.fromJson(menu1);
    print(item.menuList[0].name);
    print(item.menuList[0].iconurl);
  }

  @override
  void initState() {
    super.initState();
    readfile();
    //loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100),
        child: CustomAppBarSingleColor(
          parentContext: context,
           centerWigets: Text(postATask,style: appBarStyle),
          hight: 100,
        ),
      ),
      body: loading ?
      Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: size.convert(context, 100),
              height: size.convert(context, 100),
              child: LoadingIndicator(
            color: appColor,
            indicatorType: Indicator.ballScale,
      ),),
          ],
        ),) :_body(),
      bottomNavigationBar: CustomBottomBar(select=1),
      //body: Container(),
    );
  }

  _body(){
    //this code for align last icon in center
    int  items = item?.menuList.length;
    int  ind = items;
    if(items%3 == 1){
      ind =ind+1;
    }
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: item?.menuList == null ? Container() :StaggeredGridView.countBuilder(
          crossAxisCount: 3,
          staggeredTileBuilder: (int index)=> StaggeredTile.fit(1),
          shrinkWrap: true,
          itemCount: ind,
          scrollDirection: Axis.vertical,
          mainAxisSpacing: size.convert(context, 20),
          crossAxisSpacing: 1,
          physics: ScrollPhysics(),
          itemBuilder: (BuildContext context, int index){
            if(items%3 == 1 && index == ind-1){
              return GestureDetector(
                onTap: (){
                  print("index Number ${index-1}");
                  Navigator.push(context, PageTransition(child: postTask(), type: PageTransitionType.rightToLeft));
                },
                child: Container(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 20,),
                      Container(
                        width: size.convert(context, 57),
                        height: size.convert(context, 57),
                        decoration: BoxDecoration(
//                          image: DecorationImage(
//                              image: AssetImage(item.menuList[index-1].iconurl,
//                              ),fit: BoxFit.cover
//                          ),
                        ),
                        child:Center(child: SvgPicture.asset(item.menuList[index-1].iconurl,
                            color: appColor)),
                      ),
                      SizedBox(height: size.convert(context, 10),),
                      Container(
                        child: RichText(
                          maxLines: 1,
                          text: TextSpan(
                              text: item.menuList[index-1].name,
                              style: mainMenuStyle,
                          ),
                        ),
                      )
                    ],
                  ),

                ),
              );
            }
            if(items%3 ==1 && index == ind-2){
              return Container(
                width: size.convert(context, 10),
                height: size.convert(context, 10),

              );
            }
              return GestureDetector(
                onTap: (){
                  print("Index Number ${index}");
                  Navigator.push(context, PageTransition(child: postTask(), type: PageTransitionType.rightToLeft));
                },
                child: Container(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: size.convert(context, 20),),
                      Container(
                        width: size.convert(context, 57),
                        height: size.convert(context, 57),
                        decoration: BoxDecoration(
//                          image: DecorationImage(
//                              image: AssetImage(item.menuList[index].iconurl,
//                              ),fit: BoxFit.cover
//                          ),
                        ),
                        child:Center(child: SvgPicture.asset(item.menuList[index].iconurl,
                        color: appColor,)),
                      ),
                      SizedBox(height: size.convert(context, 10),),
                      Container(
                        child: RichText(
                          maxLines: 1,
                          text: TextSpan(
                              text: item.menuList[index].name,
                              style: mainMenuStyle
                          ),
                        ),
                      )
                    ],
                  ),

                ),
              );
          },
        ),
      );

  }


}


