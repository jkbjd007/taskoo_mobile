import 'package:taskoo/model/PostTask.dart';
import 'package:taskoo/model/offer.dart';
import 'package:taskoo/model/questions.dart';

class getTask {
  PostTask taskLite;
  List<Offers> offers;
  List<Questions> questions;

  getTask({this.taskLite, this.offers, this.questions});

  getTask.fromJson(Map<String, dynamic> json) {
    taskLite = json['taskLite'] != null
        ? new PostTask.fromJson(json['taskLite'])
        : null;
    if (json['offers'] != null) {
      offers = new List<Offers>();
      json['offers'].forEach((v) {
        offers.add(new Offers.fromJson(v));
      });
    }
    if (json['questions'] != null) {
      questions = new List<Questions>();
      json['questions'].forEach((v) {
        questions.add(new Questions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taskLite != null) {
      data['taskLite'] = this.taskLite.toJson();
    }
    if (this.offers != null) {
      data['offers'] = this.offers.map((v) => v.toJson()).toList();
    }
    if (this.questions != null) {
      data['questions'] = this.questions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}