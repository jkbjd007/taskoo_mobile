import 'package:taskoo/model/Budget.dart';
import 'package:taskoo/model/Location.dart';
import 'package:taskoo/model/user.dart';

class PostTask {
  int id;
  String title;
  String details;
  String taskStatus;
  String taskType;
  List<String> mustHaves;
  String dueDate;
  String dueTimeType;
  Budget budget;
  String category;
  locationData location;
  userData user;
  String fileInfo;
  int noOfOffers;
  String postedAtString;
  PostTask(
      {this.id,
        this.title,
        this.details,
        this.taskStatus,
        this.taskType,
        this.mustHaves,
        this.dueDate,
        this.dueTimeType,
        this.budget,
        this.category,
        this.location,
        this.user,
        this.fileInfo,
        this.noOfOffers,this.postedAtString});

  PostTask.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    details = json['details'];
    taskStatus = json['taskStatus'];
    taskType = json['taskType'];
    mustHaves = json['mustHaves'].cast<String>();
    dueDate = json['dueDate'];
    dueTimeType = json['dueTimeType'];
    budget =
    json['budget'] != null ? new Budget.fromJson(json['budget']) : null;
    category = json['category'];
    location = json['location'] != null
        ? new locationData.fromJson(json['location'])
        : null;
    user = json['user'] != null ? new userData.fromJson(json['user']) : null;
    fileInfo = json['fileInfo'];
    noOfOffers = json['noOfOffers'];
    postedAtString = json['postedAtString'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['details'] = this.details;
    data['taskStatus'] = this.taskStatus;
    data['taskType'] = this.taskType;
    data['mustHaves'] = this.mustHaves;
    data['dueDate'] = this.dueDate;
    data['dueTimeType'] = this.dueTimeType;
    if (this.budget != null) {
      data['budget'] = this.budget.toJson();
    }
    data['category'] = this.category;
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['fileInfo'] = this.fileInfo;
    data['noOfOffers'] = this.noOfOffers;
    data['postedAtString'] = this.postedAtString;
    return data;
  }
}