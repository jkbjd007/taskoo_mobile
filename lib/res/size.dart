import 'package:flutter/cupertino.dart';

class size{
  static convert(BuildContext context , double n){
    double i = n/683;
    return i*MediaQuery.of(context).size.longestSide;
  }

  static convertWithWidth(BuildContext context , double n){
    double i = n/411;
    return i*MediaQuery.of(context).size.width;
  }

  static convertWithHeight(BuildContext context , double n){
    double i = n/683;
    return i*MediaQuery.of(context).size.height;
  }

}