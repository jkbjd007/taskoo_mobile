class userData {
  int id;
  String email;
  String phone;
  String name;
  String userId;
  String imageUrl;

  userData({this.email, this.phone, this.name, this.userId, this.imageUrl,this.id});

  userData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    phone = json['phone'];
    name = json['name'];
    userId = json['userId'];
    imageUrl = json['imageUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['id'] = this.id;
    data['phone'] = this.phone;
    data['name'] = this.name;
    data['userId'] = this.userId;
    data['imageUrl'] = this.imageUrl;
    return data;
  }
}