import 'package:taskoo/model/user.dart';

class Questions {
  int id;
  int parentId;
  userData user;
  int taskId;
  String questionText;
  String postedAt;
  String postedAtString;

  Questions(
      {this.id,
        this.parentId,
        this.user,
        this.taskId,
        this.questionText,
        this.postedAt});

  Questions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    parentId = json['parentId'];
    user = json['user'] != null ? new userData.fromJson(json['user']) : null;
    taskId = json['taskId'];
    questionText = json['questionText'];
    postedAt = json['postedAt'];
    postedAtString = json['postedAtString'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['parentId'] = this.parentId;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['taskId'] = this.taskId;
    data['questionText'] = this.questionText;
    data['postedAt'] = this.postedAt;
    data['postedAtString'] = this.postedAtString;
    return data;
  }
}