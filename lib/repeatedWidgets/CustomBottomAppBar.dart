import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class CustomBottomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      decoration:BoxDecoration(
        color: Color(0xfff8f8f8),
      ),
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                IconButton(
                  onPressed: (){
                    print("1");
                  },
                  icon: Icon(Icons.description),
                ),
                Text("appointment")
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                IconButton(
                  onPressed: (){
                    print("2");
                  },
                  icon: Icon(Icons.description),
                ),
                Text("appointment")
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                IconButton(
                  onPressed: (){
                    print("3");
                  },
                  icon: Icon(Icons.description),
                ),
                Text("appointment")
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                IconButton(
                  onPressed: (){
                    print("4");
                  },
                  icon: Icon(Icons.description),
                ),
                Text("appointment")
              ],
            ),
          ),
        ],
      ),
    );
  }
}
