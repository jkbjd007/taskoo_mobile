import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:taskoo/model/PostTask.dart';
import 'package:taskoo/model/getTask.dart';
import 'package:taskoo/model/user.dart';
import 'package:taskoo/pages/browserMap.dart';
import 'package:taskoo/repeatedWidgets/CustomAppBar.dart';
import 'package:taskoo/repeatedWidgets/CustomBottomBar.dart';
import 'package:taskoo/repeatedWidgets/CustomButton.dart';
import 'package:taskoo/repeatedWidgets/CustomTextField.dart';
import 'package:taskoo/repeatedWidgets/circularImage.dart';
import 'package:taskoo/repeatedWidgets/snackbar.dart';
import 'package:taskoo/repeatedWidgets/textfieldPrice.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/serverCall.dart';
import 'package:taskoo/res/size.dart';
import 'package:page_transition/page_transition.dart';
import 'package:taskoo/res/string.dart';
import 'package:taskoo/res/style.dart';
import 'package:taskoo/pages/setUserDate.dart';
class taskDetail extends StatefulWidget {
  int taskId;
  taskDetail({this.taskId});

  @override
  _taskDetailState createState() => _taskDetailState();
}

class _taskDetailState extends State<taskDetail>
    with SingleTickerProviderStateMixin {

  TabController _tabController;
  PostTask _postTask = PostTask();
  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _offerformkey = GlobalKey<FormState>();

  /// variable for offer form validation
  bool istotalPriceEmpty = true;
  bool isdesEmpty = true;
  bool differentBudget = false;
  bool buttonEnable = false;

  ///  variable for offer form validation
  TextEditingController _totalPriceController = TextEditingController();
  TextEditingController _desController = TextEditingController();
  TextEditingController _desCommentOrQuestion = TextEditingController();
  int select = 3;
  int selectedPage = 1;
  bool loading = true;
  bool makeOffer = false;
  var api;
  getTask _getTask;
  serverRequest _serverRequest = serverRequest();
  userData currentUser = userData();
  String token = "";
  readfile() async {
    String data = await rootBundle
        .loadString("assets/propertyfile/EndPointApi.json")
        .whenComplete(() {
      setState(() {
        loading = false;
      });
    });
    api = jsonDecode(data);
    print(api);
    getDataDetail(widget.taskId);
  }
  _getUserData() async {
    currentUser.id =  await authModel.getId();
    currentUser.name = await authModel.getUserName();
    currentUser.email = await authModel.getUserEmail();
    currentUser.phone = await authModel.getUserPhone();
    currentUser.userId = await authModel.getUserId();
    currentUser.imageUrl = await authModel.getUserImageUrl();
    token = await authModel.getUserToken();
    print("User Name ="+currentUser.name);
    print("User Token = "+token);
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(vsync: this, length: 3);
    readfile();
    _getUserData();
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(size.convert(context, 100)),
        child: CustomAppBarSingleColor(
          parentContext: context,
          centerWigets: _centerText(),
          hight: size.convert(context, 100),
          leadingIcon: _selectLeadingIcon(),
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: size.convert(context, 10)),
        child: _selectBody(),
      ),
      bottomNavigationBar: makeOffer ? null : CustomBottomBar(select = 3),
    );
  }

  _centerText() {
    if (makeOffer) {
      return Text(
        makeAnOfferText,
        style: appBarStyle,
      );
    } else {
      return Text(
        taskDetailsText,
        style: appBarStyle,
      );
    }
  }

  _selectLeadingIcon() {
    if (makeOffer) {
      return GestureDetector(
        onTap: () {
          reset();
        },
        child: Icon(
          Icons.clear,
          color: Colors.black,
        ),
      );
    } else {
      return GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Icon(
          Icons.arrow_back,
          color: Colors.black,
        ),
      );
    }
  }

  _selectBody() {
    if (loading) {
      return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: size.convert(context, 100),
              height: size.convert(context, 100),
              child: LoadingIndicator(
                color: appColor,
                indicatorType: Indicator.ballScale,
              ),
            ),
          ],
        ),
      );
    } else if (makeOffer) {
      return _postedOffer("");
    } else {
      return _body();
    }
  }

  _body() {
    return Container(
      color: darkblack.withOpacity(0.05),
      child: Column(
        children: <Widget>[
          _topsearch(),
          SizedBox(
            height: size.convert(context, 10),
          ),
          _taskInfo(),
        ],
      ),
    );
  }

  _topsearch() {
    TextStyle selectedstyle = TextStyle(
      color: Colors.white,
      fontFamily: "HelveticaNeuBold",
      fontSize: size.convert(context, 10),
    );
    TextStyle unselectedstyle = TextStyle(
      color: darkblack,
      fontFamily: "HelveticaNeuBold",
      fontSize: size.convert(context, 10),
    );
    BoxDecoration selectedCont = BoxDecoration(
        borderRadius: BorderRadius.circular(size.convert(context, 10)),
        color: appColor);
    return Container(
      height: size.convert(context, 35),
      padding: EdgeInsets.only(top: size.convert(context, 15)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: RichText(
              maxLines: 1,
              text: TextSpan(
                  text: _getTask?.taskLite?.postedAtString ?? "",
                  style: postedStringTextStyle),
            ),
          ),
          Container(
            child: ListView(
              physics: ScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    padding: EdgeInsets.only(
                        top: size.convert(context, 2),
                        bottom: size.convert(context, 2),
                        left: size.convert(context, 5),
                        right: size.convert(context, 5)),
                    child: Center(
                      child: Text(
                        open,
                        style:
                            selectedPage == 0 ? selectedstyle : unselectedstyle,
                      ),
                    ),
                    decoration:
                        selectedPage == 0 ? selectedCont : BoxDecoration(),
                  ),
                ),
                GestureDetector(
                  child: Container(
                    padding: EdgeInsets.only(
                        top: size.convert(context, 2),
                        bottom: size.convert(context, 2),
                        left: size.convert(context, 5),
                        right: size.convert(context, 5)),
                    child: Center(
                      child: Text(
                        assigned,
                        style:
                            selectedPage == 1 ? selectedstyle : unselectedstyle,
                      ),
                    ),
                    decoration:
                        selectedPage == 1 ? selectedCont : BoxDecoration(),
                  ),
                ),
                GestureDetector(
                  child: Container(
                    padding: EdgeInsets.only(
                        top: size.convert(context, 2),
                        bottom: size.convert(context, 2),
                        left: size.convert(context, 5),
                        right: size.convert(context, 5)),
                    child: Center(
                      child: Text(
                        completed,
                        style:
                            selectedPage == 2 ? selectedstyle : unselectedstyle,
                      ),
                    ),
                    decoration:
                        selectedPage == 2 ? selectedCont : BoxDecoration(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _taskInfo() {
    return Expanded(
      child: SingleChildScrollView(
        physics: MediaQuery.of(context).orientation == Orientation.portrait ?NeverScrollableScrollPhysics():ScrollPhysics(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          RichText(
                            maxLines: 2,
                            text: TextSpan(
                                style: taskTitleStyle,
                                text: _getTask?.taskLite?.title ?? ""),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: size.convert(context, 15),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Container(
                                  child: circularNetworkImage(
                                    imageUrl:
                                        _getTask?.taskLite?.user?.imageUrl ??
                                            "",
                                    w: size.convert(context, 55),
                                    h: size.convert(context, 55),
                                  ),
                                ),
                                SizedBox(
                                  width: size.convert(context, 10),
                                ),
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: RichText(
                                          maxLines: 1,
                                          text: TextSpan(
                                              text: postedBy,
                                              style: TextStyle(
                                                  fontFamily:
                                                      "HelveticaNeuBold",
                                                  //fontWeight: FontWeight.bold,
                                                  fontSize:
                                                      size.convert(context, 8),
                                                  color: darkblack)),
                                        ),
                                      ),
                                      Container(
                                        child: RichText(
                                          maxLines: 1,
                                          text: TextSpan(
                                              text: _getTask
                                                      ?.taskLite?.user?.name ??
                                                  "No Name",
                                              style: TextStyle(
                                                  fontFamily: "HelveticaNeue",
                                                  // fontWeight: FontWeight.w600,
                                                  fontSize:
                                                      size.convert(context, 12),
                                                  color: darkblack)),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                  child: RichText(
                                    maxLines: 1,
                                    text: TextSpan(
                                      style: taskTitleStyle,
                                      text: currencySign +
                                          " ${_getTask?.taskLite?.budget?.amount ?? 0}",
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        child: Image.asset(
                                          "assets/icons/Icon ionic-ios-more.png",
                                          width: size.convert(context, 15),
                                          height: size.convert(context, 15),
                                        ),
                                      ),
                                      SizedBox(
                                        width: size.convert(context, 5),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            makeOffer = true;
                                          });
                                        },
                                        child: Container(
                                          padding: EdgeInsets.only(
                                              top: size.convert(context, 7),
                                              right: size.convert(context, 18),
                                              left: size.convert(context, 18),
                                              bottom: size.convert(context, 7)),
                                          decoration: BoxDecoration(
                                              color: appColor,
                                              borderRadius:
                                                  BorderRadius.circular(size
                                                      .convert(context, 20))),
                                          child: Center(
                                            child: RichText(
                                              text: TextSpan(
                                                  text: makeInOffer,
                                                  style: TextStyle(
                                                    fontFamily: "HelveticaNeue",
                                                    // fontWeight: FontWeight.w600,
                                                    color: Colors.white,
                                                    fontSize: size.convert(
                                                        context, 12),
                                                  )),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: size.convert(context, 5),
                    ),
                    Divider(
                      height: 1,
                      color: darkblack,
                    ),
                    SizedBox(
                      height: size.convert(context, 10),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            //width: size.convertWithWidth(context, 250),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  child: SvgPicture.asset(
                                    "assets/svg_Icons/map.svg",
                                    width: size.convert(context, 28),
                                    height: size.convert(context, 25),
                                    color: darkblack,
                                  ),
                                ),
                                SizedBox(
                                  width: size.convert(context, 5),
                                ),
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: RichText(
                                          text: TextSpan(
                                              text: location,
                                              style: dateAndLocationTextStyle),
                                        ),
                                      ),
                                      Container(
                                        width:
                                            size.convertWithWidth(context, 210),
                                        child: RichText(
                                          maxLines: 2,
                                          text: TextSpan(
                                              text: _getTask?.taskLite?.location
                                                      ?.vicinity ??
                                                  "",
                                              style: TextStyle(
                                                color: darkblack,
                                                fontSize:
                                                    size.convert(context, 12),
                                                fontFamily: "HelveticaNeu",
                                              )),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          if (_getTask.taskLite.location
                                                      .geometry.lat !=
                                                  null &&
                                              _getTask.taskLite.location
                                                      .geometry.lng !=
                                                  null) {
                                            print(_getTask
                                                .taskLite.location.name);

                                            Navigator.pushReplacement(
                                                context,
                                                PageTransition(
                                                    type: PageTransitionType
                                                        .downToUp,
                                                    child: browserMap(
                                                      locationdata: _getTask
                                                          .taskLite.location,
                                                    )));
                                          }
                                        },
                                        child: Container(
                                          child: RichText(
                                            text: TextSpan(
                                                text: seeOnMap,
                                                style: TextStyle(
                                                    color: appColor,
                                                    fontSize: size.convert(
                                                        context, 10),
                                                    fontFamily: "HelveticaNeu",
                                                    decoration: TextDecoration
                                                        .underline)),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                right: size.convert(context, 5)),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  child: SvgPicture.asset(
                                    "assets/svg_Icons/calendar.svg",
                                    width: size.convert(context, 21),
                                    height: size.convert(context, 25),
                                    color: darkblack,
                                  ),
                                ),
                                SizedBox(
                                  width: size.convert(context, 5),
                                ),
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        child: RichText(
                                          text: TextSpan(
                                              text: doneBy,
                                              style: dateAndLocationTextStyle),
                                        ),
                                      ),
                                      Container(
                                        child: RichText(
                                          text: TextSpan(
                                              text: changedate(
                                                  _getTask?.taskLite?.dueDate),
                                              style: TextStyle(
                                                color: darkblack,
                                                fontSize:
                                                    size.convert(context, 12),
                                                fontFamily: "HelveticaNeu",
                                              )),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {},
                                        child: Container(
                                          child: RichText(
                                            text: TextSpan(
                                                text: "Morning 7-10 AM",
                                                style: TextStyle(
                                                  color: darkblack,
                                                  fontSize:
                                                      size.convert(context, 10),
                                                  fontFamily: "HelveticaNeu",
                                                  //decoration: TextDecoration.underline
                                                )),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: size.convert(context, 10),
                    ),
                  ],
                ),
              ),
              Divider(
                height: 1,
                color: darkblack,
              ),
              SizedBox(
                height: size.convert(context, 5),
              ),
              Container(
                child: TabBar(
                    controller: _tabController,
                    labelColor: darkblack,
                    unselectedLabelColor: darkblack,
                    indicatorSize: TabBarIndicatorSize.label,
                    indicator: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                        color: Colors.white),
                    tabs: [
                      Tab(
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    detail,
                                    style: tabBarHeadTextStyle,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    offer,
                                    style:tabBarHeadTextStyle,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Tab(
                        child: Align(
                          alignment: Alignment.center,
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    question,
                                    style: tabBarHeadTextStyle,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ]),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: size.convert(context, 15)),
                  child: TabBarView(controller: _tabController, children: [
                    _Details(),
                    _offers(),
                    _Questions(),
                  ]),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _Details() {
    return Container(
      color: Colors.white,
      child: Stack(children: <Widget>[
        _getTask?.taskLite?.details == ""
            ? Column(
          children: <Widget>[
            Container(
              height: size.convert(context, 320),
              child: Center(
                child: RichText(
                  text: TextSpan(
                      text: "No detail for this task!!",
                      style: TextStyle(color: darkblack)),
                ),
              ),
            ),
          ],
        )
            : Container(height: size.convertWithHeight(context, 270),
          child: ListView.separated(
            itemBuilder: (BuildContext context, int index) {
              return Container(
                margin: EdgeInsets.symmetric(
                    horizontal: size.convert(context, 10)),
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: size.convert(context, 10),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: size.convert(context, 10)),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: RichText(
                              text: TextSpan(
                                text: _getTask?.taskLite?.details ?? "",
                                style: TextStyle(
                                  color: darkblack,
                                  fontFamily: "HelveceticeNeue",
                                  fontSize: size.convert(context, 14),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: StaggeredGridView.countBuilder(
                        crossAxisCount: 3,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            child: Image.asset(
                              "assets/icons/Rectangle 994.png",
                              height: size.convert(context, 98),
                              width: size.convert(context, 94),
                            ),
                          );
                        },
                        staggeredTileBuilder: (int index) =>
                            StaggeredTile.fit(1),
                        physics: ScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: 6,
                        scrollDirection: Axis.vertical,
                        mainAxisSpacing: size.convert(context, 10),
                      ),
                    ),
                  ],
                ),
              );
            },
            physics: ScrollPhysics(),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: 1,
            separatorBuilder: (BuildContext context, int index) {
              return SizedBox(
                height: size.convert(context, 5),
              );
            },
          ),
        ),
      ],)
    );
  }

  _offers() {
    return Container(
      color: Colors.white,
      child: Stack(children: <Widget>[
        _getTask?.offers?.length == 0
            ? Column(
          children: <Widget>[
            Container(
              height: size.convert(context, 320),
              child: Center(
                child: RichText(
                  text: TextSpan(
                      text: "No offers for this task!!",
                      style: TextStyle(color: darkblack)),
                ),
              ),
            ),
          ],
        )
            : Container(
          height: size.convertWithHeight(context, 270),
          child: ListView.separated(
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.symmetric(
                  horizontal: size.convert(context, 10)),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: size.convert(context, 10),
                  ),
                  Container(
                    child: Row(
//              crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: circularNetworkImage(
                                  imageUrl: _getTask
                                      .offers[index].user.imageUrl ??
                                      "",
                                  w: size.convert(context, 50),
                                  h: size.convert(context, 50),
                                ),
                              ),
                              SizedBox(
                                width: size.convert(context, 5),
                              ),
                              Container(
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Container(
                                      child: RichText(
                                        text: TextSpan(
                                            style: TextStyle(
                                                fontFamily: "HelveticaNeue",
                                                //fontWeight: FontWeight.w600,
                                                fontSize: size.convert(
                                                    context, 12),
                                                color: darkblack),
                                            text: _getTask.offers[index]
                                                .user.name ??
                                                ""),
                                      ),
                                    ),
                                    SizedBox(
                                      height: size.convert(context, 3),
                                    ),
                                    Container(
                                      child: SmoothStarRating(
                                          allowHalfRating: false,
                                          starCount: 5,
                                          rating: 5,
                                          size: 8.0,
                                          filledIconData: Icons.star,
                                          halfFilledIconData: Icons.blur_on,
                                          color: starColor,
                                          borderColor: starColor,
                                          spacing: 0.0),
                                    ),
                                    SizedBox(
                                      height: size.convert(context, 3),
                                    ),
                                    Container(
                                      child: RichText(
                                        text: TextSpan(
                                            style: TextStyle(
                                                fontFamily: "HelveticaNeue",
                                                // fontWeight: FontWeight.w600,
                                                fontSize: size.convert(
                                                    context, 6),
                                                color: darkblack),
                                            text: "1540 tasks completed"),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: size.convert(context, 20),
                              ),
                              Container(
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child: Row(
                                        children: <Widget>[
                                          Container(
                                            child: Image.asset(
                                              "assets/icons/Icon material-verified-user.png",
                                              height: size.convert(
                                                  context, 8.44),
                                              width: size.convert(
                                                  context, 6.91),
                                            ),
                                          ),
                                          SizedBox(
                                            width: size.convert(context, 3),
                                          ),
                                          Container(
                                            child: Image.asset(
                                              "assets/icons/Image 3.png",
                                              height:
                                              size.convert(context, 14),
                                              width:
                                              size.convert(context, 9),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      height: size.convert(context, 20),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          //padding: EdgeInsets.only(right: size.convert(context, 30)),
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: RichText(
                                  text: TextSpan(
                                    text: currencySign +
                                        " ${_getTask?.offers[index]?.amount ?? 0}",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: size.convert(context, 13),
                                      fontFamily: "HelveticaNeue",
                                      //  fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: size.convert(context, 72),
                                height: size.convert(context, 25),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        size.convert(context, 10)),
                                    color: appColor),
                                child: Center(
                                  child: Text(
                                    "accept",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: "HelveticaNeue",
                                      fontSize: size.convert(context, 15),
                                      //   fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 5),
                  ),
                  Container(
                    // padding: EdgeInsets.symmetric(horizontal: size.convert(context, 15)),
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: _getTask.offers[index]?.details ?? "",
                              style: TextStyle(
                                fontSize: size.convert(context, 10),
                                fontFamily: "HelveticaNeue",
                                // fontWeight: FontWeight.w600,
                                color: darkblack,
                              )),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 5),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            print("make comments");
                            _showDialogPostComments(true,
                                offerId: _getTask.offers[index]?.id);
                          },
                          child: Container(
                            child: Text(replyText,
                                style: TextStyle(
                                    color: appColor,
                                    fontFamily: "HelveticaNeue",
                                    fontSize: size.convert(context, 8),
                                    //  fontWeight: FontWeight.w600,
                                    decoration: TextDecoration.underline)),
                          ),
                        ),
                        Container(
                          child: RichText(
                            text: TextSpan(
                                text:
                                _getTask.offers[index].postedAtString ??
                                    "",
                                style: TextStyle(
                                  color: darkblack,
                                  fontFamily: "HelveticaNeue",
                                  fontSize: size.convert(context, 8),
                                  // fontWeight: FontWeight.w600,
                                )),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: size.convert(context, 10),
                  ),
                  Container(
                    child: ListView.separated(
                      itemBuilder: (BuildContext context, int index1) {
                        return Container(
                          margin: EdgeInsets.only(
                              right: size.convert(context, 5),
                              left: size.convert(context, 35)),
                          padding:
                          EdgeInsets.all(size.convert(context, 10)),
                          decoration: BoxDecoration(
                            color: Color(0xffF8F8F8),
                            borderRadius: BorderRadius.circular(
                                size.convert(context, 10)),
                          ),
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      child: circularNetworkImage(
                                        imageUrl: _getTask
                                            .offers[index]
                                            .comments[index1]
                                            .user
                                            .imageUrl ??
                                            "",
                                        h: size.convert(context, 25),
                                        w: size.convert(context, 25),
                                      ),
                                    ),
                                    SizedBox(
                                      width: size.convert(context, 5),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: RichText(
                                          maxLines: 3,
                                          text: TextSpan(
                                            text: _getTask
                                                .offers[index]
                                                .comments[index1]
                                                .comment ??
                                                "",
                                            style: TextStyle(
                                              color: darkblack,
                                              fontFamily: "HelveticaNeue",
                                              fontSize:
                                              size.convert(context, 10),
                                              //    fontWeight: FontWeight.w600
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: size.convert(context, 6),
                              ),
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        _getTask
                                            .offers[index]
                                            .comments[index1]
                                            .postedAtString ??
                                            "",
                                        style: TextStyle(
                                          color: darkblack,
                                          fontFamily: "HelveticaNeue",
                                          fontSize:
                                          size.convert(context, 8),
                                          //    fontWeight: FontWeight.w600
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index1) {
                        return SizedBox(
                          height: size.convert(context, 5),
                        );
                      },
                      itemCount:
                      _getTask.offers[index].comments.length ?? 0,
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                    ),
                  ),
                ],
              ),
            );
          },
          physics: ScrollPhysics(),
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: _getTask?.offers?.length ?? 0,
          separatorBuilder: (BuildContext context, int index) {
            return SizedBox(
              height: size.convert(context, 5),
            );
          },
        ),),
      ],)
    );
  }

  _Questions() {
    return Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          _getTask?.questions?.length == 0
              ? Container(
                  height: size.convert(context, 260),
                  color: whiteColor,
                  child: Center(
                    child: RichText(
                      text: TextSpan(
                          text: "No questions for this task!!",
                          style: TextStyle(color: darkblack)),
                    ),
                  ),
                )
              : SingleChildScrollView(
                child: Container(
                  height: size.convertWithHeight(context, 240),
                  margin: EdgeInsets.only(top: size.convert(context, 30),
                  bottom: size.convert(context, 0)),
                    child: ListView.separated(
                      itemBuilder: (BuildContext context, int index) {
                        print(index.toString());
                        if (index == 1 || index == 3 || index == 4) {
                          ///client ans
                          return Container(
                            margin: EdgeInsets.only(
                                right: size.convert(context, 5),
                                left: size.convert(context, 35)),
                            padding: EdgeInsets.all(size.convert(context, 10)),
                            decoration: BoxDecoration(
                              color: Color(0xffF8F8F8),
                              borderRadius: BorderRadius.circular(
                                  size.convert(context, 10)),
                            ),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        child: circularNetworkImage(
                                          imageUrl: _getTask.questions[index].user
                                                  .imageUrl ??
                                              "",
                                          w: size.convert(context, 25),
                                          h: size.convert(context, 25),
                                        ),
                                      ),
                                      SizedBox(
                                        width: size.convert(context, 5),
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: RichText(
                                            maxLines: 3,
                                            text: TextSpan(
                                              text: _getTask.questions[index]
                                                      .questionText ??
                                                  "",
                                              style: TextStyle(
                                                color: darkblack,
                                                fontFamily: "HelveticaNeue",
                                                fontSize:
                                                    size.convert(context, 10),
                                                //    fontWeight: FontWeight.w600
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: size.convert(context, 6),
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Container(
                                        child: Text(
                                          _getTask.questions[index]
                                                  .postedAtString ??
                                              "",
                                          style: TextStyle(
                                            color: darkblack,
                                            fontFamily: "HelveticaNeue",
                                            fontSize: size.convert(context, 8),
                                            //    fontWeight: FontWeight.w600
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        }

                        ///question
                        else {
                          return Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: size.convert(context, 5)),
                            padding: EdgeInsets.all(size.convert(context, 10)),
                            decoration: BoxDecoration(
                              color: Color(0xffF8F8F8),
                              borderRadius: BorderRadius.circular(
                                  size.convert(context, 10)),
                            ),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        child: circularNetworkImage(
                                          imageUrl: _getTask.questions[index].user
                                                  .imageUrl ??
                                              "",
                                          w: size.convert(context, 25),
                                          h: size.convert(context, 25),
                                        ),
                                      ),
                                      SizedBox(
                                        width: size.convert(context, 5),
                                      ),
                                      Expanded(
                                        child: Container(
                                          child: RichText(
                                            maxLines: 3,
                                            text: TextSpan(
                                              text: _getTask.questions[index]
                                                      .questionText ??
                                                  "",
                                              style: TextStyle(
                                                color: darkblack,
                                                fontFamily: "HelveticaNeue",
                                                fontSize:
                                                    size.convert(context, 10),
                                                //    fontWeight: FontWeight.w600
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: size.convert(context, 6),
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Container(
                                        child: Text(
                                          _getTask.questions[index]
                                                  .postedAtString ??
                                              "",
                                          style: TextStyle(
                                            color: darkblack,
                                            fontFamily: "HelveticaNeue",
                                            fontSize: size.convert(context, 8),
                                            //    fontWeight: FontWeight.w600
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        }
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return SizedBox(
                          height: size.convert(context, 5),
                        );
                      },
                      itemCount: _getTask?.questions?.length ?? 0,
                      physics: ScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                    ),
                  ),
              ),
              Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  print("ask question");
                  print("this is the task id = "+_getTask?.taskLite?.id.toString());
                  _showDialogPostComments(false,taskId: _getTask?.taskLite?.id );
                },
                child: Container(
                  margin: EdgeInsets.only(top: size.convert(context, 10)),
                  width: size.convert(context, 122),
                  height: size.convert(context, 20),
                  decoration: BoxDecoration(
                      borderRadius:
                      BorderRadius.circular(size.convert(context, 10)),
                      color: appColor),
                  child: Center(
                    child: Text(
                      "ask a question",
                      style: TextStyle(
                        color: Colors.white,
                        //  fontWeight: FontWeight.w600,
                        fontSize: size.convert(context, 15),
                        fontFamily: "HelveticaNeue",
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  getDataDetail(int taskId) async {
    String url = api["taskooApi"]["mainUrl"] + api["taskooApi"]["postTask"];
    try {
      print("call to get data");
      if (!mounted) return;
      setState(() {
        loading = true;
      });
      print(url);
      var res = await _serverRequest
          .getMethod("${url}/${widget.taskId}?complete=true");
      //var res = await _serverRequest.getMethod("https://api.mocklets.com/mock67958/tasks");

      if (res != null) {
        print("res");
        if (res is Response) {
          _getTask = getTask.fromJson(res.data);

          if (!mounted) return;
          setState(() {
            loading = false;
          });
        }
      }
      if (res is SocketException) {
        setState(() {
          loading = false;
        });
        CustomSnackBar.SnackBar_3Error(context, _scaffold,
            title: internetIssue);
      }
    } catch (e) {
      CustomSnackBar.SnackBar_3Error(context, _scaffold, title: internetIssue);
      print("error in = " + e.toString());
    }
  }
  String changedate(String date) {
    try {
      int day = int.parse(date.substring(0, 2));
      int month = int.parse(date.substring(3, 5));
      int year = int.parse(date.substring(5, 10));
      String newdate =
          DateFormat("EEE d MMM").format(new DateTime(year, month, day));
      return newdate;
    } catch (e) {
      return "";
    }
  }
  _postedOffer(String file) {
    if (!mounted) return;
    setState(() {
      makeOffer = true;
    });
    return Scaffold(
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.transparent,
          child: SingleChildScrollView(
            child: Container(
              margin:
                  EdgeInsets.symmetric(horizontal: size.convert(context, 10)),
              child: Form(
                key: _offerformkey,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: size.convert(context, 20),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: size.convertWithHeight(context, 260),
                            child: Text(
                              differentBudgetText,
                              style: hintsLineText20Style,
                            ),
                          ),
                          Container(
                            width: size.convertWithHeight(context, 42),
                            child: _toggleButton(),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: size.convert(context, 20),
                    ),
                    Container(
                      child: textfieldPrice(
                        hints: differentBudget? differentBudgetHintsText:differentBudgetDisableHintsText,
                        enable: differentBudget,
                        onchange: (val) {
                          print(val.toString().length.toString());
                          if (val.toString().length > 0) {
                            setState(() {
                              istotalPriceEmpty = false;
                            });
                            offersFormValidation();
                          } else {
                            setState(() {
                              istotalPriceEmpty = true;
                            });
                            offersFormValidation();
                          }
                        },
                        editingController: _totalPriceController,
                        Cheight: size.convert(context, 50),
                        Cwidth: size.convertWithWidth(context, 395),
                      ),
                    ),
                    SizedBox(
                      height: size.convert(context, 25),
                    ),
                    Container(
                      child: CustomTextField(
                        onchange: (val) {
                          print(val.toString().length.toString());
                          if (val.toString().length > 0) {
                            setState(() {
                              isdesEmpty = false;
                            });
                            offersFormValidation();
                          } else {
                            setState(() {
                              isdesEmpty = true;
                            });
                            offersFormValidation();
                          }
                        },
                        color1: darkblack,
                        textEditingController: _desController,
                        hints: offerDesHints,
                        borderwidth: 1,
                        contHight: size.convert(context, 252),
                        maxline: 10,
                      ),
                    ),
                    SizedBox(
                      height: size.convert(context, 12),
                    ),
                    GestureDetector(
                      onTap: () {
                        print("upload file click....");
                      },
                      child: CustomContainer(
                        borderwidth: 1,
                        txt: attachmentsText,
                        contHight: size.convert(context, 50),
                        widgetIcon: Container(
                          child: Image.asset(
                            "assets/icons/Icon feather-file-plus.png",
                            height: size.convert(context, 20),
                            width: size.convert(context, 16),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: size.convert(context, 20),
                    ),
                    Container(
                      child: RichText(
                        text: TextSpan(children: <TextSpan>[
                          TextSpan(
                              text:
                              byMakeOffer,
                              style: TextStyle(
                                fontSize: size.convert(context, 15),
                                fontFamily: "HelveticaNeue",
                                color: darkblack,
                                //    fontWeight: FontWeight.w600
                              )),
                          TextSpan(
                              text: agreement,
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontSize: size.convert(context, 15),
                                fontFamily: "HelveticaNeue",
                                color: appColor,
                                //    fontWeight: FontWeight.w600
                              ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {

                                }),
                        ]),
                      ),
                    ),
                    SizedBox(
                      height: size.convert(context, 25),
                    ),
                    GestureDetector(
                        onTap: () {
                          if(buttonEnable){
                            _postOffer(_getTask?.taskLite?.id);
                            reset();
                          }
                        },
                        child: CustomButton(
                          filledColor: buttonEnable ? appColor : darkblack,
                          txt: offerText,
                          hight: size.convert(context, 60),
                        )),
                  ],
                ),
              ),
            ),
          )),
    );
  }
  reset(){
    setState(() {
      makeOffer = false;
      isdesEmpty = true;
      buttonEnable = false;
      istotalPriceEmpty = true;
      _totalPriceController.text = "";
      _desController.text = "";
    });
  }
  _toggleButton() {
    return Container(
      width: size.convert(context, 42),
      height: size.convert(context, 20),
      decoration: BoxDecoration(
          color: darkblack,
          borderRadius: BorderRadius.circular(size.convert(context, 10))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          !differentBudget
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      differentBudget = !differentBudget;
                      if(differentBudget){
                        if(_totalPriceController.text =="" || _desController.text==""){
                          buttonEnable = false;
                        }
                      }
                    });
                  },
                  child: Container(
                    height: size.convert(context, 20),
                    width: size.convert(context, 25),
                    decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(size.convert(context, 10)),
                        color: whiteColor),
                  ),
                )
              : Container(
                  height: 20,
                  width: 15,
                ),
          differentBudget
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      differentBudget = !differentBudget;
                      print("NO");
                      if(_desController.text!=""){
                        buttonEnable = true;
                      }
                    });
                  },
                  child: Container(
                    height: size.convert(context, 20),
                    width: size.convert(context, 25),
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(size.convert(context, 10)),
                      color: appColor,
                    ),
                  ),
                )
              : Container(
                  height: 20,
                  width: 15,
                ),
        ],
      ),
    );
  }
  _showDialogPostComments(bool isComment, {int offerId, int taskId, parentId}) {
    return showGeneralDialog(
      context: context,
      pageBuilder: (BuildContext buildContext, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        bool isCommentOrQuestionButtonEnable = false;
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: size.convertWithHeight(context, 270),
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: size.convert(context, 20)),
                  width: size.convertWithWidth(context, 370),
                  height: size.convert(context, 200),
                  decoration: BoxDecoration(
                      color: whiteColor,
                      border: Border.all(
                        color: Colors.black,
                        width: 0.5,
                      )),
                  child: Stack(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            child: CustomTextField(
                              onchange: (val) {
                                if (val.toString().length > 0) {
                                  print(isCommentOrQuestionButtonEnable);
                                  setState(() {
                                    isCommentOrQuestionButtonEnable = true;
                                  });
                                } else {
                                  print(isCommentOrQuestionButtonEnable);
                                  setState(() {
                                    isCommentOrQuestionButtonEnable = false;
                                  });
                                }
                              },
                              color1: Colors.transparent,
                              textEditingController: _desCommentOrQuestion,
                              //error: desError,
                              hints: isComment ? commentHints : questionHints,
                              borderwidth: 1,
                              contHight: size.convert(context, 140),
                              maxline: 4,
                            ),
                          ),
                          Divider(
                            thickness: 0.7,
                            color: Colors.black,
                          ),
                          GestureDetector(
                            onTap: () {
                              if (isCommentOrQuestionButtonEnable) {
                                Navigator.pop(context);
                                _postCommentOrQuestion(
                                    isComment, offerId: offerId,taskId: taskId);
                                _desCommentOrQuestion.text = "";
                              }
                            },
                            child: Container(
                              width: size.convertWithHeight(context, 80),
                              child: CustomButton(
                                txt: "Comments",
                                filledColor: appColor,
                                hight: 30,
                                txtSize: size.convert(context, 10),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          GestureDetector(
                            onTap: (){
                              Navigator.pop(context);
                              _desCommentOrQuestion.text = "";
                            },
                              child: Container(child: Icon(Icons.clear),)),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
      barrierDismissible: true,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black.withOpacity(0.1),
      transitionDuration: const Duration(milliseconds: 200),
    );
  }
  offersFormValidation() {
    if (differentBudget) {
      if (!isdesEmpty && !istotalPriceEmpty) {
        setState(() {
          buttonEnable = true;
        });
      } else {
        setState(() {
          buttonEnable = false;
        });
      }
    }
    else if (!differentBudget) {
      if (!isdesEmpty) {
        setState(() {
          buttonEnable = true;
        });
      } else {
        setState(() {
          buttonEnable = false;
        });
      }
    }
  }
  _postCommentOrQuestion(bool isComment, {int offerId, int taskId, parentId}) async {
    String url = "";
    Map data = {};
    if (isComment) {
      url = api["taskooApi"]["mainUrl"] + api["taskooApi"]["comments"];
      data = {
        "offerId": offerId,
        "user": {
          "id": 1
        },
        "comment": _desCommentOrQuestion.text
      };
    }
    else {
      print("post questions");
      url = api["taskooApi"]["mainUrl"] + api["taskooApi"]["questions"];
      data = {
        "user": {
          "id": 1
        },
        "taskId": taskId,
        "questionText": _desCommentOrQuestion.text,
        "parentId": parentId
      };
    }
    print(url);
    try {
      if (!mounted) return;
      setState(() {
        loading = true;
      });
      var res = await _serverRequest.postMethod(url, data);
      if (res is SocketException) {
        CustomSnackBar.SnackBar_3Error(context, _scaffold,
            title: internetIssue);
        print("No internet");
        if (!mounted) return;
        setState(() {
          loading = false;
        });
      } else if (res.statusCode < 202) {
        CustomSnackBar.SnackBar_3Success(context, _scaffold,
            title: postCommentSuccess);
        getDataDetail(widget.taskId);
        if (!mounted) return;
        setState(() {
          loading = false;
        });
      } else {
        CustomSnackBar.SnackBar_3Error(context, _scaffold, title: serverError);
        if (!mounted) return;
        setState(() {
          loading = false;
        });
      }
    } catch (e) {
      print("error " + e.toString());
      CustomSnackBar.SnackBar_3Error(context, _scaffold, title: serverError);
      if (!mounted) return;
      setState(() {
        loading = false;
      });
    }
  }
  _postOffer(int taskId)async{
    try {
      String url =  api["taskooApi"]["mainUrl"] + api["taskooApi"]["offers"];
      Map data = {
        "user": {
          "id": 2
        },
        "taskId":taskId,
        "amount":_totalPriceController.text,
        "details":_desController.text
      };
      print(url);
      if (!mounted) return;
      setState(() {
        loading = true;
      });
      var res = await _serverRequest.postMethod(url, data);
      if (res is SocketException) {
        CustomSnackBar.SnackBar_3Error(context, _scaffold,
            title: internetIssue);
        print("No internet");
        if (!mounted) return;
        setState(() {
          loading = false;
        });
      }
      else if (res.statusCode < 202) {
        CustomSnackBar.SnackBar_3Success(context, _scaffold,
            title: postOfferSuccess);
        getDataDetail(widget.taskId);
        if (!mounted) return;
        setState(() {
          loading = false;
        });
      }
      else {
        CustomSnackBar.SnackBar_3Error(context, _scaffold, title: serverError);
        if (!mounted) return;
        setState(() {
          loading = false;
        });
      }
    } catch (e) {
      print("error " + e.toString());
      CustomSnackBar.SnackBar_3Error(context, _scaffold, title: serverError);
      if (!mounted) return;
      setState(() {
        loading = false;
      });
    }
  }
}
