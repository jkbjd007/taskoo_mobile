import 'package:flutter/material.dart';
import 'package:taskoo/res/size.dart';
class CustomSnackBar{

  static SnackBar_3Error(BuildContext context,_scaffoldKey,{IconData leadingIcon,String title}){
    try {
      _scaffoldKey.currentState.hideCurrentSnackBar();
    }catch(e){

    }
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Row(
          children: <Widget>[
            Icon(
              leadingIcon,
              color: Colors.white,
              size: size.convert(context, 18),
            ),
            SizedBox(
              width: size.convert(context, 5),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(top:3.0),
                child: Text(
                  title,
                  style: TextStyle(
                      fontSize: size.convert(context, 15),
                      color: Colors.white,
                      fontFamily: 'CrimsonText'),
                ),
              ),
            ),
          ],
        ),
        duration: Duration(seconds: 3),
        backgroundColor: Colors.red,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0)),
        elevation: 10.0,
        behavior: SnackBarBehavior.floating,
      ),
    );
  }

  static SnackBar_InfiniteError(BuildContext context,_scaffoldKey,{IconData leadingIcon,String title}){
    try {
      _scaffoldKey.currentState.hideCurrentSnackBar();
    }catch(e){

    }
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Row(
          children: <Widget>[
            Icon(
              leadingIcon,
              color: Colors.white,
              size: size.convert(context, 18),
            ),
            SizedBox(
              width: size.convert(context, 5),
            ),
            Expanded(
              child: Text(
                title,
                style: TextStyle(
                    fontSize: size.convert(context, 14),
                    color: Colors.white,
                    fontFamily: 'HelveticaNeue'),
              ),
            ),
          ],
        ),
        duration: Duration(hours: 3),
        backgroundColor: Colors.red,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0)),
        elevation: 10.0,
        behavior: SnackBarBehavior.floating,
      ),
    );
  }

  static SnackBar_ButtonError(BuildContext context,GlobalKey<ScaffoldState> _scaffoldKey,{IconData leadingIcon,String title,String buttonText, Function btnFun}){
    try {
      _scaffoldKey.currentState.hideCurrentSnackBar();
    }catch(e){

    }
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Row(
          children: <Widget>[
            Icon(
              leadingIcon,
              color: Colors.white,
              size: size.convert(context, 18),
            ),
            SizedBox(
              width: size.convert(context, 5),
            ),
            Expanded(
              child: Text(
                title,
                style: TextStyle(
                    fontSize: size.convert(context, 15),
                    color: Colors.white,
                    fontFamily: 'HelveticaNeue'),
              ),
            ),
          ],
        ),
        action: SnackBarAction(label: buttonText, onPressed:
        btnFun
          ,textColor: Colors.white,),
        duration: Duration(hours: 3),
        backgroundColor: Colors.red,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0)),
        elevation: 10.0,
        behavior: SnackBarBehavior.floating,
      ),
    );
  }


  static SnackBar_3Success(BuildContext context,_scaffoldKey,{IconData leadingIcon,String title}){
    try {
      _scaffoldKey.currentState.hideCurrentSnackBar();
    }catch(e){

    }
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Row(
          children: <Widget>[
            Icon(
              leadingIcon,
              color: Colors.white,
              size: size.convert(context, 18),
            ),
            SizedBox(
              width: size.convert(context, 5),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(top:3.0),

                child: Text(
                  title,
                  style: TextStyle(
                      fontSize: size.convert(context, 14),
                      color: Colors.white,
                      fontFamily: 'CrimsonText'),
                ),
              ),
            ),
          ],
        ),
        duration: Duration(seconds: 3),
        backgroundColor: Colors.green,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0)),
        elevation: 10.0,
        behavior: SnackBarBehavior.floating,
      ),
    );
  }
}