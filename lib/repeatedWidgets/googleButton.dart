import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/size.dart';
class loginButton extends StatelessWidget {
  Widget WIcon;
  Color selectColor ;
  Color borderColor ;
  double height;
  double width;
  loginButton({this.borderColor,this.width,this.height,this.selectColor,  this.WIcon});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height == null ? size.convert(context, 135) : height,
      width: width ==null ? size.convertWithWidth(context, 190): width,
      decoration: BoxDecoration(
          color: selectColor ==null ? whiteColor: selectColor,
          borderRadius: BorderRadius.circular(size.convert(context, 10)),
          border: Border.all(
            color: borderColor??darkblack,
            width: 1,
          )
      ),
      child:  WIcon == null ? Container():WIcon,
    );
  }
}
