import "package:flutter/material.dart";
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/size.dart';

class CustomButton extends StatelessWidget {
  String txt;
  Color filledColor ;
  double hight;
  double txtSize;
  CustomButton({this.txtSize,this.txt,this.hight,this.filledColor});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: hight == null ? size.convert(context, 20) : hight,
      decoration: BoxDecoration(
        color: filledColor == Null ? appColor : filledColor,
        borderRadius: BorderRadius.circular(size.convert(context, 40)),
      ),
      child: Center(
        child: Text(txt,style: TextStyle(
          color: whiteColor,
          fontSize: txtSize??size.convert(context, 30),
          fontFamily: "HelveticaNeue",
        //  fontWeight: FontWeight.w600
        ),),
      ),
    );
  }
}
