import 'package:taskoo/model/user.dart';

class otpResponse {
  userData user;
  String token;

  otpResponse({this.user, this.token});

  otpResponse.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new userData.fromJson(json['user']) : null;
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['token'] = this.token;
    return data;
  }
}