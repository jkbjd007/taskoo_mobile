import 'Geometry.dart';

class locationData {
  int id;
  String gid;
  String name;
  String placeId;
  Geometry geometry;
  String vicinity;

  locationData(
      {this.id,
        this.gid,
        this.name,
        this.placeId,
        this.geometry,
        this.vicinity});

  locationData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    gid = json['gid'];
    name = json['name'];
    placeId = json['placeId'];
    geometry = json['geometry'] != null
        ? new Geometry.fromJson(json['geometry'])
        : null;
    vicinity = json['vicinity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['gid'] = this.gid;
    data['name'] = this.name;
    data['placeId'] = this.placeId;
    if (this.geometry != null) {
      data['geometry'] = this.geometry.toJson();
    }
    data['vicinity'] = this.vicinity;
    return data;
  }
}