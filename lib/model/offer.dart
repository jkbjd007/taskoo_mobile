import 'package:taskoo/model/offerComments.dart';
import 'package:taskoo/model/user.dart';

class Offers {
  int id;
  userData user;
  int amount;
  String details;
  int taskId;
  List<offerComments> comments;
  String postedAtString;

  Offers(
      {this.id,
        this.user,
        this.amount,
        this.details,
        this.taskId,
        this.comments,this.postedAtString});

  Offers.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    user = json['user'] != null ? new userData.fromJson(json['user']) : null;
    amount = json['amount'];
    details = json['details'];
    taskId = json['taskId'];
    if (json['comments'] != null) {
      comments = new List<offerComments>();
      json['comments'].forEach((v) {
        comments.add(new offerComments.fromJson(v));
      });
    }
    postedAtString = json['postedAtString'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['amount'] = this.amount;
    data['details'] = this.details;
    data['taskId'] = this.taskId;
    if (this.comments != null) {
      data['comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    data['postedAtString'] = this.postedAtString;
    return data;
  }
}