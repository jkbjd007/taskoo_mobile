import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:taskoo/pages/profile.dart';
import 'package:taskoo/repeatedWidgets/CustomAppBar.dart';
import 'package:taskoo/repeatedWidgets/CustomBottomBar.dart';
import 'package:taskoo/res/color.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:taskoo/res/string.dart';
import 'package:taskoo/res/style.dart';
import '../main.dart';
import 'browserTasks.dart';
import 'notifications.dart';
import 'package:taskoo/model/menu.dart';
class MyTasks extends StatefulWidget {
  @override
  _MyTasksState createState() => _MyTasksState();
}

class _MyTasksState extends State<MyTasks> {
    GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  int select = 2;

    @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffold,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100),
        child: CustomAppBarSingleColor(
          parentContext: context,
          centerWigets: Text(myTask,style: appBarStyle),
          hight: 100,
        ),
      ),
      body: Container(
      ),
      bottomNavigationBar: CustomBottomBar(select=2),
    );
  }

}
