import 'package:taskoo/model/user.dart';

class offerComments {
  int id;
  String comment;
  Null offerId;
  userData user;
  String postedAt;
  String postedAtString;

  offerComments({this.id, this.comment, this.offerId, this.user, this.postedAt,this.postedAtString});

  offerComments.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    comment = json['comment'];
    offerId = json['offerId'];
    user = json['user'] != null ? new userData.fromJson(json['user']) : null;
    postedAt = json['postedAt'];
    postedAtString = json['postedAtString'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['comment'] = this.comment;
    data['offerId'] = this.offerId;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['postedAt'] = this.postedAt;
    data['postedAtString'] = this.postedAtString;
    return data;
  }
}