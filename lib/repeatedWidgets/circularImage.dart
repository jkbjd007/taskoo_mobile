import 'package:flutter/material.dart';

class circularImage extends StatelessWidget {
  String imageUrl;
  double h;
  double w;
  circularImage({this.imageUrl,this.h,this.w});
  @override
  Widget build(BuildContext context) {
    return  Container(
      height: h==null?24:h,
      width: w==null?24:w,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: imageUrl == null ? null : DecorationImage(
              image: AssetImage(imageUrl),
              fit: BoxFit.cover
          )
      ),
    );
  }
}

class circularNetworkImage extends StatelessWidget {
  String imageUrl;
  double h;
  double w;
  circularNetworkImage({this.imageUrl="https://i.picsum.photos/id/9/250/250.jpg",this.h,this.w});
  @override
  Widget build(BuildContext context) {
    return  Container(
      height: h==null?24:h,
      width: w==null?24:w,
      decoration: BoxDecoration(
        color: Colors.blueAccent,
          shape: BoxShape.circle,
          image: (imageUrl == null || imageUrl == "") ? null : DecorationImage(
              image: NetworkImage(imageUrl),
              fit: BoxFit.cover
          )
      ),
    );
  }
}



