import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/string.dart';

TextStyle appBarStyle= TextStyle(
    fontFamily: fontFamilyLight,
    fontSize: 20,
    color: Colors.black
);
TextStyle postedStringTextStyle = TextStyle(
  fontSize: 10,
  fontFamily: "HelveticaNeueMedium",
  color: darkblack,
);
TextStyle hintsLineText20Style=TextStyle(
  color: Colors.black,
  fontSize: 20,
  fontFamily: "HelveticaNeue",
  //fontWeight: FontWeight.w600,
);
TextStyle hintsLineText16Style=TextStyle(
  color: Colors.black,
  fontSize: 16,
  fontFamily: "HelveticaNeue",
  //fontWeight: FontWeight.w600,
);


TextStyle bottomUnselectTextstyle = TextStyle(
  fontFamily: "HelveticaNeue",
  fontSize: 12,
  color: darkblack,
);
TextStyle dark12Textstyle = TextStyle(
  fontFamily: "HelveticaNeue",
  fontSize: 12,
  color: darkblack,
);
TextStyle bottomSelectTextstyle = TextStyle(
  fontFamily: "HelveticaNeue",
  fontSize:  12,
  color: appColor,
);
TextStyle filterTextStyle=TextStyle(
fontSize:10,
fontFamily: "HelveticaNeueMedium",
color: Colors.white);
TextStyle tabBarHeadTextStyle=TextStyle(
fontSize: 12,
fontFamily: "HelveticaBold",
color: darkblack,
//fontWeight: FontWeight.w600,
);
TextStyle dateAndLocationTextStyle= TextStyle(
  color: darkblack,
  fontSize:8,
  fontFamily: "HelveticaNeuBold",
);
TextStyle mainMenuStyle =TextStyle(
fontSize: 12,
fontFamily: "HelveticaNeueLight",
color: Colors.black
);
TextStyle taskTitleStyle =TextStyle(
fontFamily: "HelveticaNeueLight",
fontSize:  18,
color: Colors.black
);
TextStyle taskVicinityStyle = TextStyle(
    fontSize: 10,
    fontFamily: "HelveticaNeueMedium",
    color: darkblack
);
TextStyle taskStatusStyle =TextStyle(
    fontFamily: "HelveticaNeueLight",
    fontSize: 12,
    fontWeight: FontWeight.bold,
    color: Color(0xffA3A3A3)
);
TextStyle labelStyle =TextStyle(
    color: Colors.black,
    fontFamily: "HelveticaNeue",
    fontSize: 15,
);
TextStyle distanceHintsStyle=TextStyle(
color: Colors.black,
fontFamily: "HelveticaNeue",
fontSize: 12,
);

TextStyle textFieldHintsStyle=TextStyle(
  fontSize:  20,
  fontFamily: "HelveticaNeue",
  color: darkblack,
);

TextStyle textFieldStyle=TextStyle(
  fontSize:  20,
  fontFamily: "HelveticaNeue",
  color: Colors.black,
);

TextStyle labelBlueUnderLineStyle =TextStyle(
  color: appColor,
  fontFamily: "HelveticaNeue",
  fontSize: 15,
  decoration: TextDecoration.underline
);