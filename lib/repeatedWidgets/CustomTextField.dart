import 'package:taskoo/res/color.dart';
import 'package:taskoo/res/size.dart';
import 'package:taskoo/res/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  Color color1;
  Widget iconWidget;
  Function onchange;
  Function ontap;
  String hints;
  Widget trailingIcon ;
  double borderwidth;
  int maxline;
  int maxlength;
  double contHight;
  bool enable = true;
  Function onsave;
  bool error ;
  TextEditingController textEditingController;
  Function onvalidate;
  CustomTextField({this.error = false,this.onvalidate,this.hints,this.trailingIcon,this.borderwidth,
  this.color1,this.onsave,this.textEditingController,this.ontap,this.maxline,
  this.onchange,this.enable,this.contHight,this.iconWidget,this.maxlength});

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.contHight == Null ? size.convert(context, 50) : widget.contHight,
      padding: EdgeInsets.only(right: 5, left: 5),
      decoration: BoxDecoration(
          color: whiteColor,
          border: Border.all(
            color: widget.color1 == Null ? widget.error ? Colors.red : darkblack :widget.color1,
            width: widget.borderwidth==null ? 2 : widget.borderwidth ,
          ),
          borderRadius: BorderRadius.circular(size.convert(context, 10))
      ),
      child: Row(
        children: <Widget>[
          widget.iconWidget == null ? Container() : widget.iconWidget,
          SizedBox(width: 10,),
          Expanded(
            child: TextFormField(
              //maxLengthEnforced : false,
              //autocorrect: false,
              //maxLength: maxlength == null ? 300 : maxlength,
              validator: widget.onvalidate?? (val){},
              onSaved: widget.onsave ==null ? widget.onsave :(val){
                print(val);
              },
              onChanged: widget.onchange == null ? (e){} : widget.onchange,
              enabled: widget.enable,
              onTap: widget.ontap == null ? (){} : widget.ontap,
              textInputAction: TextInputAction.done,
              maxLines: widget.maxline==null ? 1 : widget.maxline,
              controller: widget.textEditingController,
              decoration: InputDecoration(
                disabledBorder: InputBorder.none,
                border: InputBorder.none,
                hintText: widget.hints == null ? "": widget.hints,
                hintStyle: textFieldHintsStyle,

              ),
              style: textFieldStyle,
              cursorColor: Colors.black,

              //autofocus: true,
//              style: TextStyle(
//
//              ),

            ),
          ),
          widget.trailingIcon == null ? Container() : widget.trailingIcon,
        ],
      ),
    );
  }
}


class CustomContainer extends StatelessWidget {
 double contHight;
 Color color1;
 double borderwidth;
 String txt;
 Widget widgetIcon;
 Widget leadingIcon;
 MainAxisAlignment mainAxisAlignment;
 bool error ;
 CustomContainer({this.error = false ,this.leadingIcon,this.mainAxisAlignment,this.widgetIcon,this.txt,this.contHight,this.borderwidth,this.color1});
  @override
  Widget build(BuildContext context) {

    return Container(
      height: contHight == Null ? size.convert(context, 50) : contHight,
      padding: EdgeInsets.only(right: size.convert(context, 5), left: size.convert(context, 5)),
      decoration: BoxDecoration(
        color: whiteColor,
          border: Border.all(
            color: color1?? error ? Colors.red : darkblack ,
            width: borderwidth==null ? 2 : borderwidth ,
          ),
          borderRadius: BorderRadius.circular(size.convert(context, 10))
      ),
      child: Row(
        mainAxisAlignment: mainAxisAlignment == null ? MainAxisAlignment.center : mainAxisAlignment,
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                leadingIcon != null ? leadingIcon :Container(),
                Text(txt,style: TextStyle(
                    fontSize: size.convert(context, 20),
                    fontFamily: "HelveticaNeue",
                    color: darkblack,
                    fontWeight: FontWeight.w600
                ),),
              ],
            ),
          ),
          SizedBox(width: size.convert(context, 5),),
          widgetIcon == null ? Container():widgetIcon
        ],
      ),
    );
  }
}

